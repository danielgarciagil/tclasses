# README #

controls to use with FWH


### TCalex
**Build Lib**
hbmk2 tcalex.hbp -comp=<comp>

### TPlan
**Build Lib**
hbmk2 tplan.hbp -comp=<comp>

*see harbour documentation for -comp parameters*
**Build Samples**

hbmk2 <sample>.prg -comp=bcc [-L../lib -llibmysql] (optional)

verify inside fwh.hbc for correct fwh path

### Who do I talk to? ###

* daniel garcia-gil <danielgarciagil@gmail.com>