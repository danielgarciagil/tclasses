#include "fivewin.ch"
#include "calex.ch"


procedure main()
  
  local oWnd 
  local oClaex 

  SET DATE FORMAT "MM/DD/YYYY"

  define window oWnd

  define calex oCalex FIRST_DATE 4 COLOR CLR_RED
  oCalex:aGradCellNormal   = { { 1, nRGB( 0, 0, 255 ), nRGB( 255, 255, 255 ) } }
  oCalex:aGradHeaderMonth  = { { 1, nRGB( 100, 100, 100 ), nRGB( 200, 200, 200 ) } }
  oCalex:aGradHeaderWeek   = { { 1, nRGB( 100, 100, 100 ), nRGB( 200, 200, 200 ) } }
  oCalex:aGradHeaderCelDay = { { 1, nRGB( 255, 255, 255 ), nRGB( 150, 150, 150 ) } }
  oCalex:aGradHeaderCel    = { { 1, nRGB( 255, 255, 255 ), nRGB( 0, 0, 255 ) } }
  oCalex:aGradDifMonth     = { { 1, nRGB( 0, 0, 255 ), nRGB( 150, 150, 150 ) } }
  oCalex:aGradCellSelected = { { 1, nRGB( 255, 255, 0 ), nRGB( 255, 255, 255 ) } }
  oCalex:aGradLeftLabel    = { { 1, nRGB( 255, 0, 0 ), nRGB( 255, 0, 255 ) } }
  oCalex:nColorGrid        = CLR_RED
  oCalex:nColorGridToday   = CLR_GREEN

  oCalex:nInterval = 60


  //oCalex:oMonthView:bSelected = {|| msginfo("11111111") }
  oCalex:oWeekView:bSelected = {|| msginfo("222222") }
  oCalex:oDayView:bSelected = {|| msginfo("3333333") }

  oCalex:ReloadDatas()
  activate window oWnd maximized


return