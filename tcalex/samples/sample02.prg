#include "fivewin.ch"
#include "Splitter.ch"
#include "calendar.ch"
#include "calex.ch"
#include "tselex.ch"
#include "ord.ch"
#include "report.ch"

#define DEMO
#define TABLE_NAME "demo"

#xtranslate MERGE_DATE_TIME( <cDate>, <nTime> ) => hb_stot( sprintf( "%s%s", HB_DTOC( <cDate>, "YYYYMMDD" ), sprintf( "%s%s00", StrZero( int( <nTime> / 100 ), 2 ), StrZero( <nTime> - int( <nTime> / 100 ) * 100, 2 ) ) ) )

REQUEST DBFCDX
REQUEST HB_LANG_ES
REQUEST HB_CODEPAGE_ESWIN

Static nPop   := 0
Static oItems
Static aChecks

//----------------------------------------------------------------------------//

function Main()

   local oBrush
   local oMenu
   local oWnd
   local oCn


   oItems   := Array( 6 )
   aChecks  := { .F., .F., .F., .F., .F., .F. }

   HB_CDPSELECT("ESWIN")
   HB_LangSelect( "ES" )

   SET DATE FORMAT "MM/DD/YYYY"
   SET DELETE ON
   //RDDSETDEFAULT( "DBFCDX" )

   
   DEFINE BRUSH oBrush COLOR CLR_GRAY
   
   DEFINE WINDOW oWnd BRUSH oBrush MDI
   ACTIVATE WINDOW oWnd MAXIMIZED ON INIT SampleCalex():New( oWnd )
 
   oBrush:End()   
   

   //SampleCalex():New()

return nil

//----------------------------------------------------------------------------//

Function FechaServer()
Return Date()

//---------------------------------------------------//

CLASS SampleCalex

   DATA dDateStart
   DATA dDateEnd
   
   DATA oCn
   DATA cAlias
   DATA cFile
   
   DATA lOnPreNext
   
   DATA oCalex
   DATA oDtPick
   DATA oExBar   
   DATA oPanelExplorer
   DATA oPanelCalex
   DATA oWnd
   DATA oSelex
   DATA nOptSelex
   
   DATA oVSplit
   DATA lFld          INIT .F.
   DATA oFld
   
   METHOD New()

   METHOD AddDates()

   METHOD BuildCalex()  
   METHOD BuildTable()
   METHOD BuildDialog()
   METHOD BuildExplorer()
   METHOD BuildPanels()
   METHOD BuildPop( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo )
   METHOD BuildSplitterV()
   
   METHOD ChangeCheck( nPos )
   METHOD ChangeDate()
   METHOD ChangeSelex()
   METHOD ChangeView( oView, nInterval )   
   METHOD ColorStatus( dFecha )
   METHOD Connection()

   METHOD DeleteInfo( oView, nIdx )
   METHOD GetDateMonth( r, c, lView )
   METHOD Imprime( dFecha )   
   METHOD SetSize( nType, nWidth, nHeight )
   METHOD ViewDatabase()

ENDCLASS

//---------------------------------------------------//

METHOD New( oWnd ) CLASS SampleCalex
   
   local oMenu
   local oSelf    := Self
   local oBrush
   local cPath    := cFilePath( HB_ARGV( 0 ) )

   SetCurDir( cPath )
   MENU oMenu
   ENDMENU
   
   ::dDateStart := Date()
   ::dDateEnd   := Date()
   
   ::lOnPreNext := .F.

   if ::Connection()

      ::BuildTable()

      ::nOptSelex  := 3

      DEFINE BRUSH oBrush COLOR CLR_HGRAY

      if Empty( oWnd )
         DEFINE WINDOW ::oWnd MDI MENU oMenu BRUSH oBrush
      else
         DEFINE WINDOW ::oWnd MDICHILD MENU oMenu BRUSH oBrush OF oWnd
      endif

      ::BuildPanels()
      ::BuildSplitterV()
      ::BuildExplorer()
      ::BuildCalex()
      //::AddDates()


      ACTIVATE WINDOW oSelf:oWnd MAXIMIZED ;
         ON RESIZE oSelf:SetSize( nSizeType, nWidth, nHeight )

      oBrush:End()

   end

RETURN Self

//----------------------------------------------------------------------------//

METHOD BuildSplitterV() CLASS SampleCalex
   
   local oSelf    := Self
   local oParent

   if ::oWnd:ClassName() == "TMDICHILD"
      oParent := ::oWnd
   else
      oParent  := ::oWnd:oWndClient
   endif

   @ 0, oSelf:oPanelExplorer:nWidth + 2 SPLITTER oSelf:oVSplit ;
        VERTICAL ;
        PREVIOUS CONTROLS oSelf:oPanelExplorer ;
        HINDS CONTROLS oSelf:oPanelCalex ; 
        LEFT MARGIN 10 ;
        RIGHT MARGIN 80 ;
        SIZE 2, ScreenHeight() - 2 ;
        PIXEL ;
        COLOR CLR_RED ;
        OF oParent UPDATE

Return nil

//----------------------------------------------------------------------------//

METHOD AddDates( dStart, dEnd, nStart, nEnd ) CLASS SampleCalex

   local dDate
   local aGrad
   local cSql
   local oRs
   local cFront, cTo
   local cformat := "DD-MM-YYYY"

   DEFAULT dStart := CToD( "  /  /  " )
   DEFAULT dEnd   := CToD( "  /  /  " )
   DEFAULT nStart := 0
   DEFAULT nEnd   := 24 

   if nEnd >= 24
      nEnd = 23.59
   end

   nStart *= 100 
   nEnd   *= 100

   cFront = ::oCn:ValToSql( MERGE_DATE_TIME( dStart, nStart ) )
   cTo    = ::oCn:ValToSql( MERGE_DATE_TIME( dEnd, nEnd ) )

   ::oCalex:Reset()
   
   cSql := sprintf( "SELECT * FROM %s WHERE dStart >= %s and dEnd <= %s", TABLE_NAME, cFront, cTo )

   oRs   := ::oCn:Query( cSql )

   DO WHILE ! oRs:Eof()

      aGrad   := ::ColorStatus( oRs:nStatus, oRs:dStart )

      ::oCalex:LoadDates( HB_HOUR( oRs:dStart ) * 100 + HB_MINUTE( oRs:dStart ),;
                          HB_HOUR( oRs:dEnd ) * 100 + HB_MINUTE( oRs:dEnd ), ;
                          HB_CTOD( HB_TTOC( oRs:dStart, cformat ), cformat ),;
                          HB_CTOD( HB_TTOC( oRs:dEnd, cformat ), cformat ),;
                          oRs:text, ;
                          oRs:SUBJECT,;
                          oRs:id, , , , ;
                          aGrad[ 1 ], ;
                          aGrad[ 2 ], ;
                          oRs:nStatus )
                          
      oRs:Skip()
   ENDDO

RETURN nil

//---------------------------------------------------//

METHOD BuildCalex() CLASS SampleCalex
   
   local oSelf := Self

   DEFINE CALEX ::oCalex OF ::oPanelCalex ALL //TOP //LEFT //BOTTOM //RIGHT

   ::oCalex:bRClicked   := { | r, c | ::GetDateMonth( r, c ) }

   DEFINE MONTH VIEW OF ::oCalex;
          ON SELECT VIEW ( oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ), ;
                           oSelf:oDtPick:SetDate( ::dDateSelected )  ) ;
          ON SELECT DAY  oSelf:oDtPick:SetDate( dDate ) ;
          ON SELECT WEEK oSelf:oDtPick:SetDate( dDate ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ACTIVATE

   //Day

   DEFINE DAY VIEW OF ::oCalex ;
          INTERVAL ::oCalex:aOldInterval[ ::nOptSelex ] ; // 30 ;
          START HOUR 0 ;
          END HOUR 24;
          ON SELECT VIEW ( oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ), ;
                           oSelf:oDtPick:SetDate( ::dDateSelected )  ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON RIGHT CLICK ( oSelf:BuildPop( nRow, nCol, Self, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) );
          ON DELETE      ( oSelf:DeleteInfo( Self, nIdx ) )
                           
   
   //Week
   
   DEFINE WEEK VIEW OF ::oCalex ;
          INTERVAL ::oCalex:aOldInterval[ ::nOptSelex ] ; // 30 ;
          START HOUR 0 ;
          END HOUR 24;
          ON SELECT VIEW ( oSelf:oDtPick:SetDate( oLast:dDateSelected - DoW( oLast:dDateSelected ) + 1 ),;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) ) ;
          ON RIGHT CLICK ( oSelf:BuildPop( nRow, nCol, Self, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON DELETE      ( oSelf:DeleteInfo( Self, nIdx ) )
  
RETURN nil

//---------------------------------------------------//

METHOD BuildTable() CLASS SampleCalex
   local aVals      := { "00", "05", "10", "15", "20", "30", "45" }
   local nMonth, nDay, nYear, nTime1, nTime2, nStat, nOpc
   local aValues    := {}
   local aStructure := { ;
                      { "dstart"   , "T",   8, 0 },;
                      { "subject"  , "C", 100, 0 },;
                      { "text"     , "M",  10, 0 },;
                      { "idx"      , "N",  10, 0 },;
                      { "dend"     , "T",   8, 0 },;
                      { "lRemember", "L",   1, 0 },;
                      { "nStatus",   "N",   2, 0 } }

   //::oCn:DropTable( TABLE_NAME )
   if ::oCn:CreateTable( TABLE_NAME, aStructure, .T. )
      for i := 1 to 6000
         nMonth := StrZero( HB_RandomInt( 1, 12 ), 2 )
         nDay   := StrZero( HB_RandomInt( 1, if( nMonth != "02", 30, 28 ) ), 2 ) 
         nYear  := StrZero( HB_RandomInt( 2016, 2018 ), 4 )
         nTime1 := StrZero( HB_RandomInt( 0, 22 ), 2 )
         nTime2 := StrZero( HB_RandomInt( Val( nTime1 ), 23 ), 2 )
         nStat  := HB_RandomInt( 1, 10 )
         nOpc   := HB_RandomInt( 1,  7 )
         nOpc   := nOpc + 1
         nOpc   := if( nOpc > 7, 1, nOpc )
         AAdd( aValues, { ;
            sprintf( "%s-%s-%s %s:00:00", nYear, nMonth, nDay, nTime1 ),;
            sprintf( "%s-%s-%s %s:%d:00", nYear, nMonth, nDay, nTime2, 5 * nStat ),;
            "Subject: " + nDay + "/" + nMonth, ;
            "Text: " + nDay + "/" + nMonth + "/" + nYear,;
            i,;
            .t.,;
            nStat;
         } )
      next
      ::oCn:Insert( TABLE_NAME, {"dStart", "dend", "subject", "text", "idx", "lRemember",  "nStatus" }, aValues )
   endif

RETURN nil

//---------------------------------------------------//

METHOD ViewDatabase() CLASS SampleCalex

   local oRs
   local cSql := "SELECT * FROM " + TABLE_NAME

   oRs = ::oCn:Query( cSql )

   XBROWSER oRs SETUP ( oBrw:lFooter := .F. ) SHOW SLNUM //AUTOSORT // OR SHOW RECID

Return nil

//----------------------------------------------------------------------------//

METHOD BuildDialog( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) CLASS SampleCalex

   local oDlg
   local cSubject := Space( 100 )
   local cText, cSql, aValues
   local lSave    := .F.
   local lNew     := oView:oCalex:oCalInfoSelected == NIL
   local o, nID, cLastOrd
   
   DEFINE DIALOG oDlg TITLE "Adding Data" SIZE 300, 370
   
   if ! lNew
      o        := oView:oCalex:oCalInfoSelected
      cSubject := o:cSubject
      cText    := o:cText
   endif
   
   
   @ 10, 10 SAY "Date: " + DToC( dDateFrom ) OF oDlg PIXEL
   //@ 10, 10 SAY "Date: " + DToC( dDateTo ) OF oDlg PIXEL
   @ 25, 10 SAY "Time Start: [" + oView:ConvertTime( nTimeFrom, oView:lAmPm ) +;
                "] -- Time End: [" + oView:ConvertTime( nTimeTo, oView:lAmPm ) + "]" OF oDlg PIXEL
   @ 40, 10 SAY "Subject" OF oDlg PIXEL
   @ 40, 30 GET cSubject OF oDlg PIXEL SIZE 100, 10
   @ 55, 10 SAY "Text" OF oDlg PIXEL
   @ 70, 10 GET cText MEMO OF oDlg PIXEL SIZE 130, 95
   @ 170, 10 BUTTON "OK" PIXEL ACTION ( lSave := .T., oDlg:End() )
   @ 170, 100 BUTTON "CANCEL" PIXEL ACTION ( oDlg:End() )
   
   ACTIVATE DIALOG oDlg CENTERED

   IF lSave 
      if lNew 
         aValues =  { ;
             Transform( TTOS(  MERGE_DATE_TIME( dDateFrom, nTimeFrom ) ), "@R 9999-99-99 99:99:99" ),;
             Transform( TTOS(  MERGE_DATE_TIME( dDateto, nTimeto ) ), "@R 9999-99-99 99:99:99" ),;
            "Subject: " + cSubject,;
            "Text: " + cText,;
            .T.,;
            1;
         }
         ::oCn:Insert( TABLE_NAME, {"dStart", "dend", "subject", "text", "lRemember",  "nStatus" }, aValues )
      else 
         ::oCn:Update( TABLE_NAME, { "subject", "text" }, { cSubject, cText }, sprintf( "id = %d", o:nIdx  ) )
      endif
      
      ::AddDates( oView:dStart, oView:dEnd, oView:nStartHour, oView:nEndHour )      
      oView:BuildDates()      
      
   ENDIF

RETURN nil

//---------------------------------------------------//

METHOD BuildExplorer() CLASS SampleCalex

   local oPanel
   local oSelf := Self
   local nOption := 5
   
   ::oExBar := TExplorerBar():New( 0, 0, 250, 300, ::oPanelExplorer )
   
   oPanel := ::oExBar:AddPanel( Upper( "Jump To Date" ), "d:\fwh\bitmaps\32x32\calendar.bmp", 200 )
   @ 40, 15 CALENDAR ::oDtPick VAR ::dDateStart OF oPanel PIXEL SIZE 220, 157 WEEKNUMBER DAYSTATE
              
   ::oDtPick:bChange = { | o | ::ChangeDate( o ) }
   
   oPanel := ::oExBar:AddPanel( Upper( "View" ), "c:\fwh\bitmaps\32x32\view.bmp", 130 )
   oPanel:AddLink( "View Daily"  , { || ::oCalex:SetDayView(), ::ChangeSelex()  }, "d:\fwh\bitmaps\16x16\inspect.bmp" )
   oPanel:AddLink( "View Weekly" , { || ::oCalex:SetWeekView(), ::ChangeSelex() }, "d:\fwh\bitmaps\16x16\additem.bmp" )
   oPanel:AddLink( "View Monthly", { || ::oCalex:SetMonthView() }, "c:\fwh\bitmaps\16x16\calendar.bmp" )

   @ 100, 10 SELEX ::oSelex VAR nOption of oPanel PIXEL SIZE 220, 45;
      ITEMS "5", "10", "15", "20", "30", "60" ;
      GRADIENT OUTTRACK { { 1/2, nRGB( 219, 230, 244 ), nRGB( 207-50, 221-25, 255 ) }, ;
                          { 1/2, nRGB( 201-50, 217-25, 255 ), nRGB( 231, 242, 255 ) } }; 
      LINECOLORS nRGB( 237, 242, 248 ), nRGB( 141, 178, 227 );
      COLORTEXT  CLR_BLUE, CLR_RED ;
      ACTION (::oCalex:oView:SetInterval( Val( ::oSelex:aOptions[ nOption ] ) ), ::oCalex:Refresh() )

   oPanel := ::oExBar:AddPanel( Upper( "Actions" ), "d:\fwh\bitmaps\32x32\index2.bmp", 140 )
   oPanel:AddLink( "New record",  { || msginfo("New")  }, "d:\fwh\bitmaps\16x16\adddbf.bmp" )
   oPanel:AddLink( "Modify record", { || msginfo("Modify")  }, "d:\fwh\bitmaps\16x16\edit.bmp" )
   oPanel:AddLink( "Delete record",  { || msginfo("Delete")  }, "d:\fwh\bitmaps\16x16\deldbf.bmp" )
   //oPanel:AddLink( "Copy record",  { || msginfo("Copy")  }, "d:\fwh\bitmaps\16x16\copy.bmp" )
   oPanel:AddLink( "Informes",  { || oSelf:Imprime()  }, "d:\fwh\bitmaps\16x16\print.bmp" )
   oPanel:AddLink( "View Database",  { || oSelf:ViewDatabase() }, "d:\fwh\bitmaps\16x16\prg.bmp" )
   oPanel:AddLink( "Exit",  { || oSelf:oWnd:End()  }, "d:\fwh\bitmaps\16x16\exit2.bmp" )

   oPanel := ::oExBar:AddPanel( Upper( "INFO" ), "d:\fwh\bitmaps\32x32\notes.bmp", 52 )
   oPanel:AddLink( "Info",  { || XBrowse( ::oCalex:hCalInfo ) }, "d:\fwh\bitmaps\16x16\prg.bmp" )

   ::oPanelExplorer:oClient := ::oExBar
   ::oPanelExplorer:oClient:bMouseWheel := { || MsgInfo( "ok" ) }

Return nil

//---------------------------------------------------//

METHOD BuildPanels() CLASS SampleCalex

   local oBrush
   local oParent

   if ::oWnd:ClassName() == "TMDICHILD"
      oParent  := ::oWnd
   else
      oParent  := ::oWnd:oWndClient
   endif
   ::oPanelExplorer := TPanel():New( 1, 0, ::oWnd:nHeight, 280, oParent )
   ::oPanelCalex    := TPanel():New( 1, ::oPanelExplorer:nWidth + 6, ::oWnd:nHeight, ;
                                     ::oWnd:nWidth - ::oPanelExplorer:nWidth + 6, oParent )

   DEFINE BRUSH oBrush COLOR CLR_WHITE
   ::oPanelExplorer:SetBrush( oBrush )
   ::oPanelCalex:SetBrush( oBrush )
   oBrush:End()
   
   if ::lFld
   @ 1, 1 FOLDEREX ::oFld ;
         PROMPTS "Calex" OF ::oPanelCalex ;
         TAB HEIGHT 22 ;
         SIZE  2 *::oPanelCalex:nWidth - 70, 2* ::oPanelCalex:nHeight - 266 ; 
         PIXEL //FONT oThis:oFontXF
   endif

Return nil

//---------------------------------------------------//

METHOD ChangeSelex() CLASS SampleCalex
   
   local cInterval := AllTrim( Str( ::oCalex:oView:nInterval ) )
   local nAt       := AScan( ::oSelex:aOptions, cInterval )

   if nAt != 0  .and. ::oSelex:nOption != nAt
      ::oSelex:SetOption( nAt )
      ::oSelex:Refresh()
   endif 

Return nil

//---------------------------------------------------//

METHOD SetSize( nType, nWidth, nHeight ) CLASS SampleCalex

   if nWidth != nil 
      ::oPanelExplorer:Move( , , , nHeight )
      ::oPanelCalex:Move( , , nWidth - ( ::oPanelExplorer:nRight + 6 ), nHeight )
   endif
   
Return nil

//---------------------------------------------------//

METHOD GetDateMonth( r, c, lView ) CLASS SampleCalex

   local dDate
   local aPos      := {}
   local oCalInfo
   local cStrDay
   local aInfo
   local hDays
   local aDates    := {}
   local nH
   local nW
   DEFAULT lView   := .T.

   if ::oCalex:oView:ClassName() == Upper( "TMonthView" )
      aPos   := ::oCalex:oView:GetPosition( r, c )
      dDate  := ::oCalex:oView:GetDateFromPos( aPos[ 1 ], aPos[ 2 ] ) 
      //XBrowse( ::oCalex:oView:GetCoorFromPos( aPos[ 1 ], aPos[ 2 ] ) )
      cStrDay := DToS( dDate )
      hDays = hb_HASH()
      for each oCalInfo in ::oCalex:hCalInfo
         aInfo = {}
#ifdef __XHARBOUR__
         oCalInfo := oCalInfo:Value
#endif
         if oCalInfo:dStart == dDate
            if hb_HHASKEY( hDays, cStrDay )
               aInfo := hb_HGET( hDays, cStrDay )
            endif
            AAdd( aInfo, oCalInfo )
            ASort( aInfo, , ,{ | o1, o2 | o1:nStart < o2:nStart } )
            //AAdd( aDates, { oCalInfo:nIdx, oCalInfo:dStart, oCalInfo:dEnd, oCalInfo:nStart, oCalInfo:nEnd, RTrim( oCalinfo:cSubject ), oCalInfo:nStatus } )
            AAdd( aDates, { oCalInfo:nStart, oCalInfo:nStart + oCalInfo:nEnd, RTrim( oCalinfo:cSubject ), oCalInfo:nStatus, oCalInfo:nIdx } )
            //XBrowse( aInfo )
            hb_HSET( hDays, cStrDay, aInfo )
         endif
      next
   endif

   if lView .and. !Empty( aDates )
      //XBrowse( aDates ) //hDays )
      //XBrowse( ::oCalex:oCalInfo:aCoords )
      if !Empty( ::oCalex:oCalInfo )
      //r  := ::oCalex:oCalInfo:aCoords[ 1 ]
      //c  := ::oCalex:oCalInfo:aCoords[ 2 ]
      
      //nH := Int( ::oCalex:oCalInfo:aCoords[ 3 ] / 2 ) - 1
      //nW := Int( ::oCalex:oCalInfo:aCoords[ 4 ] / 2 ) + 1
      nH := ::oCalex:oView:nRowHeight - 21
      nW := ::oCalex:oView:nColWidth  - 3

      APopupBrowse( aDates, ::oCalex:oWnd, r - 1, c - 9, nH, nW, ;
          { | o | ( o:lHeader := .F. ) }, 5 )
          //{ | o | ( o:cHeaders := { "HourS", "HourE", "Subject", "St", "Id" } ) }, 1 )
          //{ | o | ( o:cHeaders := { "Id", "Start", "End", "HourS", "HourE", "Subject", "St" } ) }, 1 )
      endif
   endif

Return dDate

//----------------------------------------------------------------------------//

METHOD BuildPop( nRow, nCol, oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) CLASS SampleCalex

   local oMenu
   local oSelf    := Self
   local lNew     := oView:oCalex:oCalInfoSelected == NIL
   local oCalex   := oView:oCalex

   MENU oMenu POPUP
      MENUITEM If( lNew, "New appointment", "Modify appointment" ) ;
         ACTION oSelf:BuildDialog( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo )
      SEPARATOR
      if ! lNew                         
         MENUITEM "Delete appointment"  ACTION If( MsgYesNo( "Are you sure?" ), oView:oCalex:DelCalInfo(), )
      endif
      
      MENUITEM "Today" ACTION ( oView:SetDate( date() ), If( oView:IsKindOf( "TWEEKVIEW" ),;
                                 oView:oCalex:SetWeekView(), oView:oCalex:SetDayView() ) )
      SEPARATOR

      MENUITEM  oItems[ 1 ] PROMPT "5  minutes" CHECKED ACTION ( oSelf:ChangeView( oView,  5 ) )
      MENUITEM  oItems[ 2 ] PROMPT "10 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 10 ) )
      MENUITEM  oItems[ 3 ] PROMPT "15 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 15 ) )
      MENUITEM  oItems[ 4 ] PROMPT "20 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 20 ) )
      MENUITEM  oItems[ 5 ] PROMPT "30 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 30 ) )
      MENUITEM  oItems[ 6 ] PROMPT "60 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 60 ) )

   ENDMENU
   ::ChangeCheck()

   ACTIVATE POPUP oMenu OF oView:oCalex AT nRow, nCol

Return nil	

//----------------------------------------------------------------------------//

METHOD ChangeView( oView, nInterval ) CLASS SampleCalex

   local oCalex   := oView:oCalex
   local nPos     := AsCan( ::oCalex:aOldInterval, nInterval )

   if !Empty( nPos )
      oCalex:oWeekView:SetInterval( nInterval )
      oCalex:oWeekView:Refresh()
      oCalex:oDayView:SetInterval( nInterval )
      oCalex:oDayView:Refresh()
      ::ChangeCheck( nPos )
   endif

Return nil	

//----------------------------------------------------------------------------//

METHOD ChangeCheck( nPos ) CLASS SampleCalex

   local x
   DEFAULT nPos  := 0

   if !Empty( nPos )
      aChecks[ nPos ] := ! aChecks[ nPos ]
      For x = 1 to Len( oItems )
         if nPos != x
            if aChecks[ x ]
               aChecks[ x ] := ! aChecks[ x ]
            endif
         else
            if ! aChecks[ x ]
               aChecks[ x ] := ! aChecks[ x ]
            endif
         endif
         oItems[ x ]:SetCheck( aChecks[ x ] )
      Next x
      if !Empty( ::oSelex )
         ::ChangeSelex()
      endif
   else
      For x = 1 to Len( oItems )
          oItems[ x ]:SetCheck( aChecks[ x ] )
      Next x
   endif
Return nil

//---------------------------------------------------//

METHOD DeleteInfo( oView, nIdx ) CLASS SampleCalex
   local cSql 

   cSql = sprintf( "DELETE FROM %s WHERE id = %d", TABLE_NAME, nIdx )
   
   ::oCn:Execute( cSql )

RETURN NIL

//---------------------------------------------------//

METHOD ChangeDate( oDatePick ) CLASS SampleCalex

   ::oCalex:oView:SetDate( oDatePick:GetDate() )
   
   if ::oCalex:oView:IsKindOf( "TMONTHVIEW" )
      ::oCalex:SetMonthView()
   elseif ::oCalex:oView:IsKindOf( "TWEEKVIEW" )
      ::oCalex:SetWeekView()
   else
      ::oCalex:SetDayView()
   endif
   
RETURN NIL

//----------------------------------------------------------------------------//

METHOD ColorStatus( nType, dFecha ) CLASS SampleCalex

   local aGrad
   local nClrT     := CLR_BLACK
   DEFAULT nType   := 0

   DO CASE
      case nType = 1
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }
         nClrT     := CLR_BLACK
      case nType = 2
         aGrad := { { 1, RGB(0x00,0x80,0xff) , RGB(0x00,0x80,0xff) } }
         nClrT     := CLR_BLACK
      case nType = 3
         aGrad := { { 1, RGB(0xff,0xff,0x80), RGB(0xff,0xff,0x80) } }
         nClrT     := CLR_BLACK
      case nType = 4
         aGrad := { { 1, RGB(0xff,0x00,0x00), RGB(0xff,0x00,0x00) } }
         nClrT := CLR_WHITE
      case nType = 5
         aGrad := { { 1, RGB(0xc0,0xc0,0xc0), RGB(0xc0,0xc0,0xc0) } }
         nClrT     := CLR_BLACK
      case nType = 6
         aGrad := { { 1, METRO_ORANGE, METRO_ORANGE } }
         nClrT     := CLR_BLACK
      case nType = 7
         aGrad := { { 1, METRO_LIME, METRO_LIME } }
         nClrT     := CLR_BLACK
      case nType = 8
         aGrad := { { 1, METRO_CRIMSON, METRO_CRIMSON } }
         nClrT     := CLR_BLACK
      case nType = 9
         aGrad := { { 1, METRO_OLIVE, METRO_OLIVE } }
         nClrT     := CLR_BLACK
      case nType = 10
         aGrad := { { 1, METRO_STEEL, METRO_STEEL } }
         nClrT     := CLR_BLUE
      Otherwise
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }      
  ENDCASE

return { aGrad, nClrT }

//----------------------------------------------------------------------------//
METHOD Connection() CLASS  SampleCalex
   
   local oCn

   FWCONNECT oCn HOST "localhost" USER "tcalex" PASSWORD "1234" DB "tcalex"
   if oCn == nil
      ? "Connect Fail"
      return nil
   else
      ::oCn = oCn
      ::buildTable()
   endif

RETURN oCn != NIL

//----------------------------------------------------------------------------//

METHOD Imprime( dFecha ) CLASS SampleCalex

   local oRep
   local oFont1, oFont2, acor, oDlg1, oGet1, oGet2, oGet3, oBot1, oBot2
   local lRep    := .f.
   local dDesde
   local dHasta
   local mestado := 1
   local oRs
   
   DEFAULT dFecha := ::oCalex:dDateSelected
   
   dDesde := dFecha
   dHasta := dFecha

   // Defino los distintos tipos de letra
   DEFINE FONT oFont1 NAME "ARIAL" SIZE 0,-10
   DEFINE FONT oFont2 NAME "ARIAL" SIZE 0,-10 BOLD
   
   
   DEFINE DIALOG oDlg1 TITLE "Informe de Citas" FROM 03,15 TO 13,70 //       OF oApp:oWnd
   
   @ 07, 05 SAY "Estado:"               OF oDlg1 PIXEL SIZE 50,12 RIGHT
   @ 22, 05 SAY "Desde Fecha:"          OF oDlg1 PIXEL SIZE 50,12 RIGHT
   @ 37, 05 SAY "Hasta Fecha:"          OF oDlg1 PIXEL SIZE 50,12 RIGHT
   
   @ 05, 60 COMBOBOX oGet1 VAR mestado OF oDlg1 SIZE 60,12 PIXEL ;
      ITEMS { "Todas", "Solo pendientes", "Solo Realizadas" }
   @ 20, 60 GET oGet2 VAR dDesde  OF oDlg1 PIXEL
   @ 35, 60 GET oGet3 VAR dHasta  OF oDlg1 PIXEL VALID ( dHasta >= dDesde )
   @ 50,40 BUTTON oBot1 PROMPT "&Imprimir" OF oDlg1 SIZE 30,10 ;
           ACTION ( lRep := .t., oDlg1:End() ) PIXEL
   @ 50,90 BUTTON oBot2 PROMPT "&Cancelar" OF oDlg1 SIZE 30,10 ;
           ACTION ( lRep := .f., oDlg1:End() ) PIXEL
   ACTIVATE DIALOG oDlg1 CENTER
   
   IF !lRep
      RETURN nil
   ENDIF
   
   cFront = ::oCn:ValToSql( MERGE_DATE_TIME( dDesde, 0 ) )
   cTo    = ::oCn:ValToSql( MERGE_DATE_TIME( dHasta, 2359 ) )
   
   cSql := sprintf( "SELECT * FROM %s WHERE dStart >= %s and dEnd <= %s order by dstart", TABLE_NAME, cFront, cTo )

   oRs   := ::oCn:Query( cSql )

   REPORT oRep ;
       TITLE "Citas de " + ALLTRIM( "TCalex" )  + " del " + DTOC( dDesde ) + " al " + DTOC( dHasta ) LEFT ;
       FONT  oFont1, oFont2 ;
       HEADER OemToAnsi( "Sample01" ) RIGHT ;
       FOOTER "Hoja: " + STR( oRep:nPage, 3 ) ,"Fecha: " + DToc( DATE() ) LEFT ;
       PREVIEW CAPTION "Citas"

   oRep:bSkip   = {|| oRs:skip() }
   oRep:bWhile  = {|| ! oRs:Eof }
   oRep:bEnd    = {|| oRs:GoTop() }

   COLUMN TITLE "Id"      DATA oRs:id     SIZE 07 FONT 1
   COLUMN TITLE "Subject" DATA Left( oRs:subject, 40 ) SIZE 20 FONT 1
   COLUMN TITLE "Fecha"   DATA oRs:dStart SIZE 08 FONT 2
   COLUMN TITLE "Desde"   DATA SPRINTF( "%s:%s", strzero( HB_HOUR( oRs:dStart ), 2), strzero( HB_MINUTE( oRs:dStart ), 2)) SIZE 05 FONT 1 RIGHT
   COLUMN TITLE "Hasta"   DATA SPRINTF( "%s:%s", strzero( HB_HOUR( oRs:DEND ), 2), strzero( HB_MINUTE( oRs:DEND ), 2))  SIZE 05 FONT 1 RIGHT
   COLUMN TITLE "Asunto"  DATA oRs:text    SIZE 25 FONT 1 MEMO

   // Digo que el titulo lo escriba con al letra 2
   oRep:oTitle:aFont[1] := {|| 2 }
   oRep:oTitle:aFont[1] := {|| 2 }
   oRep:bInit           := {|| oRs:GoTop() }

   END REPORT

   ACTIVATE REPORT oRep 

   RELEASE oFont1
   RELEASE oFont2 

RETURN nil

//----------------------------------------------------------------------------//

function APopupBrowse( aValue, oWnd, nRow, nCol, nH, nW, bInit, nColumn )

   local oDlg
   local oBrw
   local bGetValid
   local uOrgl
   local uRet
   local aPoint    := { nRow, nCol }
   local cFace     := "Calibri"
   local nHFont    := -10
   local oFont

   DEFAULT oWnd    := GetWndDefault()
   DEFAULT nW      := 500
   DEFAULT nH      := 180
   DEFAULT nColumn := 1

   if !Empty( aValue )
      aPoint = ClientToScreen( oWnd:hWnd, aPoint )
      DEFINE FONT oFont        NAME cFace SIZE 0, nHFont
      DEFINE DIALOG oDlg OF oWnd STYLE WS_POPUP ;
         SIZE nW, nH
      
      ACTIVATE DIALOG oDlg NOWAIT ;
         ON INIT oDlg:SetPos( aPoint[ 1 ], aPoint[ 2 ] ) ;
         VALID ( oFont:End(), .T. )
      
      @ 0, 0 XBROWSE oBrw DATASOURCE aValue AUTOCOLS AUTOSORT ;
         SIZE oDlg:nWidth, oDlg:nHeight OF oDlg PIXEL NOBORDER FONT oFont
      
      WITH OBJECT oBrw
         :lRecordSelector  := .F.
         :lHScroll         := .F.
         :lFlatStyle       := .T.
         :lFastDraw        := .T.
         :AutoFit()
         :bClrStd    := { || { ColorStatus( oBrw:aRow[ 4 ] )[ 2 ], ColorStatus( oBrw:aRow[ 4 ] )[ 1 ] } }
      
         if bInit != nil
            Eval( bInit, oBrw )
         endif
         :CreateFromCode()
         
         :PostMsg( WM_SETFOCUS )
         :bKeyDown   := { | nKey | If( nKey == VK_RETURN, ( Eval( oBrw:bChange ), oDlg:End() ), ;
                                   If( nKey == VK_ESCAPE, oDlg:End(), ) ) }
         :bLDblClick := { | r, c | ( oDlg:End() ) }
         :bChange    := { || uRet := oBrw:aCols[ nColumn ]:Value }
         :bLButtonUp := { | nRow, nCol | Eval( oBrw:bChange ) }
         //:Seek( oGet:GetText() )
      END
   endif

return uRet

//----------------------------------------------------------------------------//

Function ColorStatus( nType, dFecha )

   local aGrad
   local nClrT     := CLR_BLACK
   DEFAULT nType   := 0

   DO CASE
      case nType = 1
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }
         nClrT     := CLR_BLACK
      case nType = 2
         aGrad := { { 1, RGB(0x00,0x80,0xff) , RGB(0x00,0x80,0xff) } }
         nClrT     := CLR_BLACK
      case nType = 3
         aGrad := { { 1, RGB(0xff,0xff,0x80), RGB(0xff,0xff,0x80) } }
         nClrT     := CLR_BLACK
      case nType = 4
         aGrad := { { 1, RGB(0xff,0x00,0x00), RGB(0xff,0x00,0x00) } }
         nClrT := CLR_WHITE
      case nType = 5
         aGrad := { { 1, RGB(0xc0,0xc0,0xc0), RGB(0xc0,0xc0,0xc0) } }
         nClrT     := CLR_BLACK
      case nType = 6
         aGrad := { { 1, METRO_ORANGE, METRO_ORANGE } }
         nClrT     := CLR_BLACK
      case nType = 7
         aGrad := { { 1, METRO_LIME, METRO_LIME } }
         nClrT     := CLR_BLACK
      case nType = 8
         aGrad := { { 1, METRO_CRIMSON, METRO_CRIMSON } }
         nClrT     := CLR_BLACK
      case nType = 9
         aGrad := { { 1, METRO_OLIVE, METRO_OLIVE } }
         nClrT     := CLR_BLACK
      case nType = 10
         aGrad := { { 1, METRO_STEEL, METRO_STEEL } }
         nClrT     := CLR_BLUE
      Otherwise
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }      
  ENDCASE

return { aGrad, nClrT }

//----------------------------------------------------------------------------//
