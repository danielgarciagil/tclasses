//TWeekV.prg

#include "fivewin.ch"
#include "calex.ch"


CLASS TWeekView FROM TCalex

   DATA oCalex

   DATA nRowHeight    // Row Height
   DATA nRowCount     // total rows in grid
   DATA hDays
   DATA nRowDown, nColDown
   DATA nLastRow
   DATA nLastCol
   DATA aSelectedRow

   METHOD New()

   METHOD GoNextWeek()
   METHOD GoPrevWeek()

   METHOD GridHeight() INLINE ::nRowCount * ::nRowHeight

   METHOD LButtonDown( nRow, nCol, nKeyFlags )
   METHOD LButtonUp( nRow, nCol, nKeyFlags )
   METHOD LDblClick( nRow, nCol, nKeyFlags ) VIRTUAL

   METHOD MouseMove( nRow, nCol, nKeyFlags )
   METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos )

   METHOD Paint()
   METHOD PaintHeader( hDC )

   METHOD Resize( nType, nWidth, nHeight )

   METHOD SetDatas()

   METHOD SetDate( dDate ) INLINE ::dDateSelected := If( dDate == NIL, CToD( "  /  /  "), dDate ),;
                                  ::oCalex:oDayView:dDateSelected := dDate,;
                                  ::oCalex:oMonthView:SetDate( dDate )

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( oCalex ) CLASS TWeekView
   
   ::oCalex = oCalex

   ::SetDatas()

RETURN Self

//----------------------------------------------------------------------------//

METHOD GoNextWeek() CLASS TWeekView
   local dTemp := ::dDateSelected - DoW( ::dDateSelected ) + 8
   ::dStart  = dTemp
   ::dEnd   = ::dStart + 6
return dTemp

//----------------------------------------------------------------------------//

METHOD GoPrevWeek() CLASS TWeekView
   local dTemp := ::dDateSelected - DoW( ::dDateSelected ) - 6
   ::dStart  = dTemp
   ::dEnd   = ::dStart + 6
return dTemp

//----------------------------------------------------------------------------//

METHOD LButtonDown( nRow, nCol, nKeyFlags ) CLASS TWeekView

   ::LButtonDownView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//


METHOD LButtonUp( nRow, nCol, nKeyFlags ) CLASS TWeekView

   ::LButtonUpView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//


METHOD MouseMove( nRow, nCol, nKeyFlags ) CLASS TWeekView

   ::MouseMoveView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//

METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos ) CLASS TWeekView

   local aPos := { nYPos, nXPos }

   if ::lSBVisible
      if nDelta < 0
         ::VScrollSkip( 40 )
      else
         ::VScrollSkip( -40 )
      endif
   endif

return nil

//----------------------------------------------------------------------------//

METHOD Paint( hDC ) CLASS TWeekView

   local nGridHeight 	:= ::GridHeight()
   local nGridWidth  	:= ::GridWidth()

   local n, nColStep
   local nModCol
   local hOldFont, nOldClr
   local aDataArea := Array( 4 )
   local aLabelArea
   local hNext, hPrev, cTop
   local dFirstDateWeek, dLastDateWeek
   local aCell, nFrom := 0, nTo := 0


   FillRect( hDC, GetClientRect( ::hWnd ), ::oBrush:hBrush )

   aDataArea[ 1 ] := ::nTopMargin - ::nVirtualTop
   aDataArea[ 2 ] := ::nLeftMargin + ::nLeftLabelWidth
   aDataArea[ 3 ] := nGridHeight + ::nTopMargin - ::nVirtualTop
   aDataArea[ 4 ] := nGridWidth + ( ::nLeftMargin + ::nLeftLabelWidth )


   WndBox2007( hDC, aDataArea[ 1 ],;
               aDataArea[ 2 ], ;
               aDataArea[ 3 ], ;
               aDataArea[ 4 ], ;
               ::nColorGrid )


   GradientFill( hDC, aDataArea[ 1 ] + 1, aDataArea[ 2 ] + 1, aDataArea[ 3 ] - 1, aDataArea[ 4 ], ::aGradCellNormal )


   if ::lCaptured
      if ::nAtRow < ::nRowDown
         nFrom = ::nAtRow
         nTo   = ::nRowDown
      elseif ::nAtRow > ::nRowDown
         nFrom = ::nRowDown
         nTo   = ::nAtRow
      else
         nFrom = ::nRowDown
         nTo   = ::nRowDown
      endif
      ::aSelectedRow = {}
      for n = nFrom to nTo
         aCell = ::GetCoorFromPos( n, ::nColDown )
         GradientFill( hDC, aCell[ 1 ], aCell[ 2 ], aCell[ 3 ], aCell[ 4 ], ::aGradCellSelected )
         AAdd( ::aSelectedRow, { n, ::nColDown } )
      next
   else
      for each n in ::aSelectedRow
         aCell = ::GetCoorFromPos( n[ 1 ], n [ 2 ] )
         GradientFill( hDC, aCell[ 1 ], aCell[ 2 ], aCell[ 3 ], aCell[ 4 ], ::aGradCellSelected )
      next
   endif

   hOldFont = SelectObject( hDC, ::oFont:hFont )
   nOldClr  = SetTextColor( hDC, ::nClrText )

   ::PaintHorzLinesWithLeftLabels( hDC )

   nColStep = Int( nGridWidth / ::nDays )
   nModCol = nGridWidth % ::nDays

   //Vertical lines
   for n = 1 to 6
      ::Line( hDC, ::nTopMargin - ::nDNameHeight,;
              ::nLeftMargin + ::nLeftLabelWidth + ( n * nColStep ),;
              nGridHeight + ::nTopMargin,;
              ::nLeftMargin + ::nLeftLabelWidth + ( n * nColStep ),;
              ::nColorGrid )
   next

   ::PaintCalInfo( hDC )

   FillRect( hDC, {0, 0, ::nTopMargin, aDataArea[ 4 ] + 1}, ::oBrush:hBrush )

   WndBox2007( hDC, ::nTopMargin - ::nDNameHeight,;
               aDataArea[ 2 ], ;
               ::nTopMargin, ;
               aDataArea[ 4 ], ;
               ::nColorGrid )

   //Header Vertical lines
   for n = 1 to 6
      ::Line( hDC, ::nTopMargin - ::nDNameHeight,;
              ::nLeftMargin + ::nLeftLabelWidth + ( n * nColStep ),;
              ::nTopMargin,;
              ::nLeftMargin + ::nLeftLabelWidth + ( n * nColStep ),;
              ::nColorGrid )
   next


   ::PaintHeader( hDC )

   if ::lOverNext
      hNext = ::hNextItemo
   else
      hNext = ::hNextItem
   endif

   if ::lOverPrev
      hPrev = ::hPrevItemo
   else
      hPrev = ::hPrevItem
   endif

   SelectObject( hDC, hOldFont )
   SetTextColor( hDC, nOldClr )

   DrawTransparent( hDC, hNext, ROWITEM - ::nBmpRows, COLNEXT )   // fjhg

   DrawTransparent( hDC, hPrev, ROWITEM - ::nBmpRows, COLPREV )   // fjhg
   dFirstDateWeek = ::GetFirstDateWeek()
   dLastDateWeek  = ::GetlastDateWeek()
   if Month( dLastDateWeek ) != Month( dFirstDateWeek )
      if Year( dLastDateWeek ) != Year( dFirstDateWeek )
         cTop = CMonth( dFirstDateWeek ) + " " + StrZero( Day( dFirstDateWeek ), 2 ) + ", " + Str( Year( dFirstDateWeek ) ) + " - "
         cTop += CMonth( dLastDateWeek ) + " " + StrZero( Day( dLastDateWeek ), 2 ) + ", " + Str( Year( dLastDateWeek ) )
      else
         cTop = CMonth( dFirstDateWeek ) + " " + StrZero( Day( dFirstDateWeek ), 2 ) + " - "
         cTop += CMonth( dLastDateWeek ) + " " + StrZero( Day( dLastDateWeek ), 2 ) + ", " + Str( Year( dLastDateWeek ) )
      endif
   else
      cTop = CMonth( dFirstDateWeek ) + " " + StrZero( Day( dFirstDateWeek ), 2 ) + " - " + StrZero( Day( dLastDateWeek ), 2 ) + ", " + Str( Year( dFirstDateWeek ) )
   endif

   hOldFont = SelectObject( hDC, ::oFontTop:hFont )
   nOldClr  = SetTextColor( hDC, ::nClrText )
   DrawTextTransparent( hDC, cTop, ;
                       { 1 , COLNEXT + BMPITEMW + 10, ROWITEM + GetTextHeight() , ::nWidth }, nOR( DT_SINGLELINE, DT_VCENTER ) )
   SelectObject( hDC, hOldFont )
   SetTextColor( hDC, nOldClr )


return nil

//----------------------------------------------------------------------------//

METHOD PaintHeader( hDC ) CLASS TWeekView

   local dFirstDateWeek := ::GetFirstDateWeek()
   local n, nStyle
   local aArea := Array( 4 )
   local cText, nOldClr, cDay
   local nColStep, nModCol
   local nGridWidth := ::GridWidth()
   local nOldFont, nTextWidth
   local aGradHead, dTemp
   local dFecha := Date()  //FechaServer()    // fjhg sustituye a Date()

   nColStep = Int( nGridWidth / ::nDays )
   nModCol = nGridWidth % ::nDays

   nStyle = nOR( DT_SINGLELINE, DT_CENTER, DT_VCENTER )

   for n = 0 to 6
      aArea[ 1 ] = ::nTopMargin - ::nDNameHeight
      aArea[ 2 ] = n * nColStep + ::nLeftMargin + ::nLeftLabelWidth + 1
      aArea[ 3 ] = ::nTopMargin
      aArea[ 4 ] = aArea[ 2 ] + nColStep + If( n == 6, nModCol, 0 ) - 1
      dTemp = dFirstDateWeek + n
      if dTemp == dFecha      // fjhg
         aGradHead := ::aGradHeaderCelDay
      else
         aGradHead := ::aGradHeaderWeek // ::aGradHeaderCel
      endif

      GradientFill( hDC, Max( 1, aArea[ 1 ] + 1 ), aArea[ 2 ], aArea[ 3 ] - 1, aArea[ 4 ], aGradHead )

      cText = OEMtoANSI(CDoW( dFirstDateWeek + n  ))
      if aArea[ 4 ] - aArea[ 2 ] - If( n == 6, nModCol, 0 ) < 100
         cText = SubStr( cText, 1, 3 )
      endif
      nOldClr  = SetTextColor( hDC, ::nClrText )
      cDay = Str( Day( dFirstDateWeek + n  ), 3 )

      nOldClr := SetTextColor( hDC, ::nClrText )
      
      //Display character day week
      if aArea[ 4 ] - aArea[ 2 ] - If( n == 6, nModCol, 0 ) > 60
         DrawTextTransparent( hDC, cText , aArea, nStyle )
      endif

      //Display numeric day
      nOldFont = SelectObject( hDC, ::oFontHeader:hFont )
      DrawTextTransparent( hDC, cDay, ;
      { aArea[ 1 ], aArea[ 2 ], aArea[ 3 ], aArea[ 2 ] + GetTextWidth( hDC, cDay, ::oFontHeader:hFont ) }, nStyle )
      SetTextColor( hDC, nOldClr )
      SelectObject( hDC, nOldFont )

      if dTemp == dFecha
         WndBox2007( hDC, aArea[ 1 ],;
               aArea[ 2 ] - 1, ;
               aArea[ 3 ], ;
               aArea[ 4 ], ;
               ::nColorGridToday )
      endif

   next


return nil

//----------------------------------------------------------------------------//

METHOD Resize( nType, nWidth, nHeight ) CLASS TWeekView


   local nGridWidth, nColStep, nModCol
   local aCoor := { NIL, NIL }
   local nAdjCol, nStart, lRet := .T., cStrInterval
   local nLastRight, nLastLeft := 0
   local hInfo, oCalInfo, aInfo, aTempInfo,  nTot, n, j := 2
   local nModAdj, nDay, nMaxRight
   local dFirstDateWeek := ::GetFirstDateWeek()

   nGridWidth := ::GridWidth()
   nColStep = Int( nGridWidth / ::nDays )
   nModCol = nGridWidth % ::nDays - 1

   if ! Empty( ::hDays ) //.and. nGridWidth > 70

      for each hInfo in ::hDays
#ifdef __XHARBOUR__
         hInfo = hInfo:Value
#endif
         for each aInfo in hInfo
#ifdef __XHARBOUR__
            aInfo = aInfo:Value
#endif
            for each oCalInfo in aInfo

               j = 2
               if oCalInfo:nFlags == FLAGS_START

                  nDay       = oCalInfo:dStart - dFirstDateWeek + 1
                  nTot       := ::CheckChildren( oCalInfo )
                  nAdjCol    = nColStep / nTot
                  nModAdj    = nColStep % nTot
                  nStart     = oCalInfo:nStart
                  aCoor[ 1 ] = ::GetCoorFromTime( oCalInfo:nStart, oCalInfo:dStart )
                  oCalInfo:Move( , aCoor[ 1 ][ 2 ] - 1, aCoor[ 1 ][ 2 ] + nAdjCol- 1 )
                  nLastRight = oCalInfo:aCoords[ CI_RIGHT ]
                  nLastLeft  = oCalInfo:aCoords[ CI_LEFT ]
                  nMaxRight  = nColStep * nDay + ::nLeftLabelWidth + ::nLeftMargin
                  while lRet
                     cStrInterval := ::GetStrInterval( nStart )
                     if hb_HHASKEY( hInfo, cStrInterval )
                        aTempInfo = hb_HGET( hInfo, cStrInterval )
                        if Len( aTempInfo ) > 0
                           for n = j to Len( aTempInfo )
                              if nAND( aTempInfo[ n ]:nFlags, FLAGS_RIGHT ) == FLAGS_RIGHT
                                 nTot --
                                 aTempInfo[ n ]:Move( , nLastRight, nLastRight + nAdjCol + If( nLastRight + nAdjCol + nModAdj == nMaxRight, nModAdj, 0 ) )
                              elseif nAND( aTempInfo[ n ]:nFlags, FLAGS_DOWN ) == FLAGS_DOWN
                                 aTempInfo[ n ]:Move( , nLastLeft, nLastLeft + nAdjCol  + If( nLastLeft + nAdjCol + nModAdj == nMaxRight, nModAdj, 0 ) )
                              endif

                              lRet = nAND( aTempInfo[ n ]:nFlags, FLAGS_END ) != FLAGS_END
                              nLastRight = aTempInfo[ n ]:aCoords[ CI_RIGHT ]
                              nLastLeft  = aTempInfo[ n ]:aCoords[ CI_LEFT ]
                           next
                        endif
                     endif
                     nStart = ::NextInterval( nStart )
                     j = 1
                     lRet = nStart > 0 .and. lRet
                  enddo
                  lRet = .T.
               endif
            next
         next
      next
   endif


RETURN nil

//-- -----

METHOD SetDatas() CLASS TWeekView

   ::oCalex:SetDatas( Self )
   ::nLeftLabelWidth    = 50
   ::nLeftMargin        = 0
   ::nRowHeight         = 24
   ::nDNameHeight       = 25
   ::lAmPm              = .F.
   ::oVScroll           = ::oCalex:oVScroll
   ::aGradCellSelected  = { { 1, nRGB( 0, 0, 100 ), nRGB( 0, 0, 100 ) } }
   ::dDateSelected      = CToD( "  /  /  ")
   ::nVirtualTop        = 0
   ::nDays              = 7
   ::lCaptured          = .F.
   ::nRowDown           = 0
   ::nColDown           = 0
   ::nAtRow             = 0
   ::nAtCol             = 0
   ::aSelectedRow       = {}

RETURN NIL
