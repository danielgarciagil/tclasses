//TDayView.prg

#include "fivewin.ch"
#include "calex.ch"


CLASS TDayView FROM TCalex

   DATA oCalex

   DATA nRowHeight    // Row Height
   DATA nRowCount     // total rows in grid
   DATA nRowDown
   DATA nColDown
   DATA nLastRow
   DATA nLastCol

   DATA aSelectedRow

   METHOD New()

   METHOD GoNextDay()
   METHOD GoPrevDay()

   METHOD GridHeight() INLINE ::nRowCount * ::nRowHeight

   METHOD LButtonDown( nRow, nCol, nKeyFlags )
   METHOD LButtonUp( nRow, nCol, nKeyFlags )
   METHOD LDblClick( nRow, nCol, nKeyFlags ) VIRTUAL


   METHOD MouseMove( nRow, nCol, nKeyFlags )
   METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos )

   METHOD Paint()
   METHOD PaintHeader( hDC )


   METHOD Resize( nType, nWidth, nHeight ) //INLINE ::CheckScroll()

   METHOD SetDatas()

   METHOD SetDate( dDate ) INLINE ::dDateSelected := dDate,;
                                  ::oCalex:oMonthView:SetDate( dDate ),;
                                  ::oCalex:oWeekView:SetDate( dDate )



ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( oCalex ) CLASS TDayView
   
   ::oCalex = oCalex

   ::SetDatas()

RETURN Self

//----------------------------------------------------------------------------//

METHOD GoNextDay() CLASS TDayView
return ++::dDateSelected

//----------------------------------------------------------------------------//

METHOD GoPrevDay() CLASS TDayView
return --::dDateSelected

//----------------------------------------------------------------------------//

METHOD LButtonDown( nRow, nCol, nKeyFlags ) CLASS TDayView

   ::LButtonDownView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//


METHOD LButtonUp( nRow, nCol, nKeyFlags ) CLASS TDayView

   ::LButtonUpView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//


METHOD MouseMove( nRow, nCol, nKeyFlags ) CLASS TDayView


   ::MouseMoveView( nRow, nCol, nKeyFlags )

return nil

//----------------------------------------------------------------------------//

METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos ) CLASS TDayView

   local aPos := { nYPos, nXPos }

   if ::lSBVisible
      if nDelta < 0
         ::VScrollSkip( 40 )
      else
         ::VScrollSkip( -40 )
      endif
   endif

return nil

//----------------------------------------------------------------------------//

METHOD Paint( hDC ) CLASS TDayView

   local nGridHeight   := ::GridHeight()
   local nGridWidth    := ::GridWidth()
   local cTime         := ""
   local hOldFont
   local nOldClr
   local nTop
   local nBottom
   local aDataArea     := Array( 4 )
   local aLabelArea
   local hNext
   local hPrev
   local cTop
   local dFirstDateWeek
   local dLastDateWeek
   local nFrom
   local nTo
   local n
   local aCell


   FillRect( hDC, GetClientRect( ::hWnd ), ::oBrush:hBrush )

   aDataArea[ 1 ] := ::nTopMargin - ::nVirtualTop
   aDataArea[ 2 ] := ::nLeftMargin + ::nLeftLabelWidth
   aDataArea[ 3 ] := nGridHeight + ::nTopMargin - ::nVirtualTop
   aDataArea[ 4 ] := nGridWidth + ( ::nLeftMargin + ::nLeftLabelWidth )


   WndBox2007( hDC, aDataArea[ 1 ],;
               aDataArea[ 2 ], ;
               aDataArea[ 3 ], ;
               aDataArea[ 4 ], ;
               ::nColorGrid )


   GradientFill( hDC, aDataArea[ 1 ] + 1, aDataArea[ 2 ] + 1, aDataArea[ 3 ] - 1, aDataArea[ 4 ], ::aGradCellNormal )

   if ::aOverCell != NIL
      GradientFill( hDC, ::aOverCell[ 1 ], ::aOverCell[ 2 ], ::aOverCell[ 3 ], ::aOverCell[ 4 ], ::aGradCellSelected )
   endif

   hOldFont = SelectObject( hDC, ::oFont:hFont )
   nOldClr  = SetTextColor( hDC, ::nClrText )


   ::PaintHorzLinesWithLeftLabels( hDC )

   GradientFill( hDC, 0, 0, ::nTopMargin - 1, aDataArea[ 4 ] + 1, ::aGradCellNormal )

   if ::lCaptured
      if ::nAtRow < ::nRowDown
         nFrom = ::nAtRow
         nTo   = ::nRowDown
      elseif ::nAtRow > ::nRowDown
         nFrom = ::nRowDown
         nTo   = ::nAtRow
      else
         nFrom = ::nRowDown
         nTo   = ::nRowDown
      endif
      ::aSelectedRow = {}
      for n = nFrom to nTo
         aCell = ::GetCoorFromPos( n, ::nColDown )
         GradientFill( hDC, aCell[ 1 ], aCell[ 2 ], aCell[ 3 ] - 1, aCell[ 4 ], ::aGradCellSelected )
         AAdd( ::aSelectedRow, { n, ::nColDown } )
      next
   else
      for each n in ::aSelectedRow
         aCell = ::GetCoorFromPos( n[ 1 ], n [ 2 ] )
         GradientFill( hDC, aCell[ 1 ], aCell[ 2 ], aCell[ 3 ] - 1, aCell[ 4 ], ::aGradCellSelected )
      next
   endif


   ::PaintCalInfo( hDC )

   FillRect( hDC, {0, 0, ::nTopMargin, aDataArea[ 4 ] + 1}, ::oBrush:hBrush )
  
   WndBox2007( hDC, ::nTopMargin - ::nDNameHeight,;
               aDataArea[ 2 ], ;
               ::nTopMargin, ;
               aDataArea[ 4 ], ;
               ::nColorGrid )


   ::PaintHeader( hDC )

   if ::lOverNext
      hNext = ::hNextItemo
   else
      hNext = ::hNextItem
   endif

   if ::lOverPrev
      hPrev = ::hPrevItemo
   else
      hPrev = ::hPrevItem
   endif

   SelectObject( hDC, hOldFont )
   SetTextColor( hDC, nOldClr )

   DrawTransparent( hDC, hNext, ROWITEM - ::nBmpRows, COLNEXT )   // fjhg

   DrawTransparent( hDC, hPrev, ROWITEM - ::nBmpRows, COLPREV )   // fjhg

   dFirstDateWeek = ::GetFirstDateWeek()
   cTop = CMonth( dFirstDateWeek ) + " " + StrZero( Day( dFirstDateWeek ), 2 ) + ", " + Str( Year( dFirstDateWeek ) )
   hOldFont = SelectObject( hDC, ::oFontTop:hFont )
   nOldClr  = SetTextColor( hDC, ::nClrText )
   DrawTextTransparent( hDC, cTop, ;
                       { 1, COLNEXT + BMPITEMW + 10, ROWITEM + GetTextHeight(), ::nWidth }, nOR( DT_SINGLELINE, DT_VCENTER ) )
   SelectObject( hDC, hOldFont )
   SetTextColor( hDC, nOldClr )


return nil

//----------------------------------------------------------------------------//

METHOD PaintHeader( hDC ) CLASS TDayView

   local dFirstDateWeek := ::GetFirstDateWeek()
   local n, nStyle
   local aArea := Array( 4 )
   local cText, nOldClr, cDay
   local nColStep, nModCol
   local nGridWidth := ::GridWidth()
   local nOldFont, nTextWidth
   local aGradHead, dTemp
   local dFecha := Date()    //FechaServer()    // fjhg sustituye a Date()

   nColStep = Int( nGridWidth / ::nDays )
   nModCol = nGridWidth % ::nDays

   nStyle = nOR( DT_SINGLELINE, DT_CENTER, DT_VCENTER )


   aArea[ 1 ] = ::nTopMargin - ::nDNameHeight
   aArea[ 2 ] = ::nLeftMargin + ::nLeftLabelWidth + 1
   aArea[ 3 ] = ::nTopMargin
   aArea[ 4 ] = aArea[ 2 ] + nGridWidth - 1

   dTemp = dFirstDateWeek

   if dTemp == dFecha      // fjhg
      aGradHead = ::aGradHeaderCelDay
   else
      aGradHead := ::aGradHeaderCel
   endif

   GradientFill( hDC, Max( 1, aArea[ 1 ] + 1 ), aArea[ 2 ], aArea[ 3 ] - 1, aArea[ 4 ], aGradHead )
   cText = OEMtoANSI(CDoW( dFirstDateWeek ))
   if aArea[ 4 ] - aArea[ 2 ] < 100
      cText = SubStr( cText, 1, 3 )
   endif
   nOldClr  = SetTextColor( hDC, ::nClrText )
   cDay = Str( Day( dFirstDateWeek ), 2 )

   //Display character day week
   if aArea[ 4 ] - aArea[ 2 ]  > 60
      DrawTextTransparent( hDC, cText , aArea, nStyle )
   endif

   //Display numeric day
   nOldFont = SelectObject( hDC, ::oFontHeader:hFont )
   DrawTextTransparent( hDC, cDay, ;
   { aArea[ 1 ], aArea[ 2 ], aArea[ 3 ], aArea[ 2 ] + GetTextWidth( hDC, cDay, ::oFontHeader:hFont ) }, nStyle )
   SetTextColor( hDC, nOldClr )
   SelectObject( hDC, nOldFont )


return nil

//----------------------------------------------------------------------------//

METHOD Resize( nType, nWidth, nHeight ) CLASS TDayView


   local nGridWidth, nColStep, nModCol
   local aCoor := { NIL, NIL }
   local nAdjCol, nStart, lRet := .T., cStrInterval
   local nLastRight, nLastLeft := 0
   local hInfo, oCalInfo, aInfo, aTempInfo,  nTot, n, j := 2
   local nModAdj, nDay, nMaxRight
   local dFirstDateWeek := ::GetFirstDateWeek()


   nGridWidth := ::GridWidth()
   nColStep   := Int( nGridWidth / ::nDays )
   nModCol    := 0

   if ! Empty( ::hDays ) //.and. nGridWidth > 70

      for each hInfo in ::hDays
#ifdef __XHARBOUR__
         hInfo = hInfo:Value
#endif
         for each aInfo in hInfo //oCalInfo in aInfo
#ifdef __XHARBOUR__
            aInfo = aInfo:Value
#endif
            for each oCalInfo in aInfo
               j = 2
               if oCalInfo:nFlags == FLAGS_START

                  nDay       = oCalInfo:dStart - dFirstDateWeek + 1
                  nTot       := ::CheckChildren( oCalInfo )
                  nAdjCol    = nColStep / nTot
                  nModAdj    = nColStep % nTot
                  nStart     = oCalInfo:nStart
                  aCoor[ 1 ] = ::GetCoorFromTime( oCalInfo:nStart, oCalInfo:dStart )
                  oCalInfo:Move( , aCoor[ 1 ][ 2 ] - 1, aCoor[ 1 ][ 2 ] + nAdjCol- 1 )
                  nLastRight = oCalInfo:aCoords[ CI_RIGHT ]
                  nLastLeft  = oCalInfo:aCoords[ CI_LEFT ]
                  nMaxRight  = nColStep * nDay + ::nLeftLabelWidth + ::nLeftMargin
                  while lRet
                     cStrInterval := ::GetStrInterval( nStart )
                     if hb_HHASKEY( hInfo, cStrInterval )
                        aTempInfo = hb_HGET( hInfo, cStrInterval )
                        if Len( aTempInfo ) > 0
                           for n = j to Len( aTempInfo )
                              if nAND( aTempInfo[ n ]:nFlags, FLAGS_RIGHT ) == FLAGS_RIGHT
                                 nTot --
                                 aTempInfo[ n ]:Move( , nLastRight, nLastRight + nAdjCol + If( nLastRight + nAdjCol + nModAdj == nMaxRight, nModAdj, 0 ) )
                              elseif nAND( aTempInfo[ n ]:nFlags, FLAGS_DOWN ) == FLAGS_DOWN
                                 aTempInfo[ n ]:Move( , nLastLeft, nLastLeft + nAdjCol  + If( nLastLeft + nAdjCol + nModAdj == nMaxRight, nModAdj, 0 ) )
                              endif

                              lRet = nAND( aTempInfo[ n ]:nFlags, FLAGS_END ) != FLAGS_END
                              nLastRight = aTempInfo[ n ]:aCoords[ CI_RIGHT ]
                              nLastLeft  = aTempInfo[ n ]:aCoords[ CI_LEFT ]
                           next
                        endif
                     endif
                     nStart := ::NextInterval( nStart )
                     j = 1
                     lRet = nStart > 0 .and. lRet
                  enddo
                  lRet = .T.

               endif
            next
         next
      next
   endif

RETURN nil

//----------------------------------------------------------------------------//

METHOD SetDatas() CLASS TDayView

   ::oCalex:SetDatas( Self )
   ::nLeftLabelWidth    = 50
   ::nLeftMargin        = 0
   ::nRowHeight         = 24
   ::nDNameHeight       = 25
   ::lAmPm              = .F.
   ::oVScroll           = ::oCalex:oVScroll
   ::dDateSelected      = CToD( "  /  /  " )
   ::nVirtualTop        = 0
   ::nDays              = 1
   ::aGradCellSelected  = { { 1, nRGB( 0, 0, 100 ), nRGB( 0, 0, 100 ) } }
   ::lCaptured          = .F.
   ::nRowDown           = 0
   ::nColDown           = 0
   ::nAtRow             = 0
   ::nAtCol             = 0
   ::aSelectedRow       = {}

return nil

//----------------------------------------------------------------------------//


