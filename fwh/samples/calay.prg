#include "fivewin.ch"
#include "calex.ch"
#include "layout.ch"

procedure main()
  local oWnd 
  local hLay     := {=>}
  local hBrush   := {=>}
  local hButtons := { => }
  local oCalex, oBrw

  define brush hBrush["blue"] color CLR_BLUE
  define brush hBrush["green"] color CLR_GREEN
  define brush hBrush["red"] color CLR_RED
  define brush hBrush["cyan"] color CLR_CYAN
  define brush hBrush["yellow"] color CLR_YELLOW

  define window oWnd title "Layout SAMPLE"

  define layout hLay["MAIN"] of oWnd

  define horizontal layout hLay["HOR_1"] of hLay["MAIN"] size 150

  define vertical layout hLay["VER_1_HOR_1"] of hLay["HOR_1"] size 90
  define vertical layout hLay["VER_2_HOR_1"] of hLay["HOR_1"] size 90

  @ 0,0 button hButtons["one"] prompt "BUTTON ONE" of hLay["VER_1_HOR_1"]
  @ 0,0 button hButtons["two"] prompt "BUTTON TWO" of hLay["VER_2_HOR_1"]

  set layout control hButtons["one"] of  hLay["VER_1_HOR_1"]


  set layout control hButtons["two"] of  hLay["VER_2_HOR_1"]

  define horizontal layout hLay["HOR_2"] of hLay["MAIN"]

  hLay["HOR_2"]:setBrush( hBrush["green"] )

  define calex oCalex of hLay["HOR_2"]
  set layout control oCalex of  hLay["HOR_2"]

  define horizontal layout hLay["HOR_3"] of hLay["MAIN"] size 300

  USE citas NEW SHARED ALIAS "citas"

   @ 0, 0 XBROWSE oBrw OF hLay["HOR_3"] PIXEL AUTOCOLS ALIAS alias() NOBORDER

   oBrw:nMarqueeStyle    = MARQSTYLE_HIGHLWIN7
   oBrw:CreateFromCode()

   set layout control oBrw of hLay["HOR_3"]

  //@ 0,0 button hButtons["one"] prompt "BUTTON ONE" of hLay["V1"]
  //set layout control hButtons["one"] of  hLay["V1"]

  activate window oWnd maximized

  killBrush( hBrush )

return


procedure killBrush( hBrush )
  
  local hItemBrush


  for each hItemBrush in hBrush
    hItemBrush:end()
  end

return

/*


  define layout hLay["MAIN"] of oWnd

  define vertical layout hLay["V1"] of hLay["MAIN"] size 75
  define vertical layout hLay["V2"] of hLay["MAIN"]

  hLay["V1"]:SetBrush( hBrush["blue"] )

  define horizontal layout hLay["H1"] of hLay["V2"] size 150
  define horizontal layout hLay["H2"] of hLay["V2"]

  hLay["H1"]:SetBrush( hBrush["red"] )

  define vertical layout hLay["V3"] of hLay["H2"] 
  define vertical layout hLay["V4"] of hLay["H2"] size 200 

  hLay["V3"]:SetBrush( hBrush["yellow"] )
  hLay["V4"]:SetBrush( hBrush["green"] )


  define vertical layout hLay["V5"] of hLay["H1"] 
  define vertical layout hLay["V6"] of hLay["H1"] 
  define vertical layout hLay["V7"] of hLay["H1"] 

  hLay["V5"]:SetBrush( hBrush["red"] )
  hLay["V6"]:SetBrush( hBrush["green"] )
  hLay["V7"]:SetBrush( hBrush["blue"] )

  define vertical layout hLay["V8"] of hLay["V5"] 
  define vertical layout hLay["V9"] of hLay["V5"] 
  define vertical layout hLay["V10"] of hLay["V5"] 
  define vertical layout hLay["V11"] of hLay["V5"] 

  @ 0,0 button o


------


  define horizontal layout hLay["HOR_1"] of hLay["MAIN"] size 150

  define vertical layout hLay["VER_1_HOR_1"] of hLay["HOR_1"] size 90
  define vertical layout hLay["VER_2_HOR_1"] of hLay["HOR_1"] size 90

  @ 0,0 button hButtons["one"] prompt "BUTTON ONE" of hLay["VER_1_HOR_1"]
  @ 0,0 button hButtons["two"] prompt "BUTTON TWO" of hLay["VER_2_HOR_1"]

  set layout control hButtons["one"] of  hLay["VER_1_HOR_1"]
  set layout control hButtons["two"] of  hLay["VER_2_HOR_1"]

  define horizontal layout hLay["HOR_2"] of hLay["MAIN"]

  hLay["HOR_2"]:setBrush( hBrush["green"] )

  define calex oCalex of hLay["HOR_2"]
  set layout control oCalex of  hLay["HOR_2"]

  define horizontal layout hLay["HOR_3"] of hLay["MAIN"] size 300

  USE citas NEW SHARED ALIAS "citas"

   @ 0, 0 XBROWSE oBrw OF hLay["HOR_3"] PIXEL AUTOCOLS ALIAS alias() NOBORDER

   oBrw:nMarqueeStyle    = MARQSTYLE_HIGHLWIN7
   oBrw:CreateFromCode()

   set layout control oBrw of hLay["HOR_3"]


*/