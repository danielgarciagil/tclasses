#include "fivewin.ch"
#include "Splitter.ch"
#include "calendar.ch"
#include "calex.ch"
#include "tselex.ch"
#include "ord.ch"
#include "report.ch"

#define DEMO

REQUEST DBFCDX
REQUEST HB_LANG_ES
REQUEST HB_CODEPAGE_ESWIN

Static nPop   := 0
Static oItems
Static aChecks

//----------------------------------------------------------------------------//

function Main()

   local oBrush
   local oMenu
   local oWnd

   oItems   := Array( 6 )
   aChecks  := { .F., .F., .F., .F., .F., .F. }

   HB_CDPSELECT("ESWIN")
   HB_LangSelect( "ES" )

   SET DATE FORMAT "MM/DD/YYYY"
   SET DELETE ON
   //RDDSETDEFAULT( "DBFCDX" )

   
   DEFINE BRUSH oBrush COLOR CLR_GRAY
   DEFINE WINDOW oWnd BRUSH oBrush MDI
   ACTIVATE WINDOW oWnd MAXIMIZED ;
      ON INIT SampleCalex():New( oWnd )
 
   oBrush:End()   
   

   //SampleCalex():New()

return nil

//----------------------------------------------------------------------------//

Function FechaServer()
Return Date()

//---------------------------------------------------//

CLASS SampleCalex

   DATA dDateStart
   DATA dDateEnd
   
   DATA cAlias
   DATA cFile
   
   DATA lOnPreNext
   
   DATA oCalex
   DATA oDtPick
   DATA oExBar   
   DATA oPanelExplorer
   DATA oPanelCalex
   DATA oWnd
   DATA oSelex
   DATA nOptSelex
   
   DATA oVSplit
   DATA lFld          INIT .F.
   DATA oFld
   
   METHOD New()

   METHOD AddDates()

   METHOD BuildCalex()  
   METHOD BuildDbf()
   METHOD BuildDialog()
   METHOD BuildExplorer()
   METHOD BuildPanels()
   METHOD BuildPop( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo )
   METHOD BuildSplitterV()
   
   METHOD ChangeCheck( nPos )
   METHOD ChangeDate()
   METHOD ChangeSelex()
   METHOD ChangeView( oView, nInterval )   
   METHOD ColorStatus( dFecha )
   METHOD DeleteInfo( oView, nIdx )
   METHOD GetDateMonth( r, c, lView )
   METHOD Imprime( dFecha )   
   METHOD SetSize( nType, nWidth, nHeight )
   METHOD ViewDatabase()

ENDCLASS

//---------------------------------------------------//

METHOD New( oWnd ) CLASS SampleCalex
   
   local oMenu
   local oSelf    := Self
   local oBrush
   local cPath    := cFilePath( HB_ARGV( 0 ) )

   SetCurDir( cPath )
   MENU oMenu
   ENDMENU
   
   ::dDateStart := Date()
   ::dDateEnd   := Date()
   
   ::lOnPreNext := .F.
   
   ::cAlias     := "citas"
   ::cFile      := Upper( cPath + ::cAlias + ".dbf" )

   ::BuildDbf()

   ::nOptSelex  := 3

   DEFINE BRUSH oBrush COLOR CLR_HGRAY

   if Empty( oWnd )
      DEFINE WINDOW ::oWnd MDI MENU oMenu BRUSH oBrush
   else
      DEFINE WINDOW ::oWnd MDICHILD MENU oMenu BRUSH oBrush OF oWnd
   endif

   //::lFld            := .T.

   ::BuildPanels()
   ::BuildSplitterV()
   ::BuildExplorer()
   ::BuildCalex()
   //::AddDates()


   ACTIVATE WINDOW oSelf:oWnd MAXIMIZED ;
      ON RESIZE oSelf:SetSize( nSizeType, nWidth, nHeight ) ;
      VALID ( DbCloseAll(), .T. )
 
   oBrush:End()

RETURN Self

//----------------------------------------------------------------------------//

METHOD BuildSplitterV() CLASS SampleCalex
   
   local oSelf    := Self
   local oParent

   if ::oWnd:ClassName() == "TMDICHILD"
      oParent := ::oWnd
   else
      oParent  := ::oWnd:oWndClient
   endif

   @ 0, oSelf:oPanelExplorer:nWidth + 2 SPLITTER oSelf:oVSplit ;
        VERTICAL ;
        PREVIOUS CONTROLS oSelf:oPanelExplorer ;
        HINDS CONTROLS oSelf:oPanelCalex ; 
        LEFT MARGIN 10 ;
        RIGHT MARGIN 80 ;
        SIZE 2, ScreenHeight() - 2 ;
        PIXEL ;
        COLOR CLR_RED ;
        OF oParent UPDATE

Return nil

//----------------------------------------------------------------------------//

METHOD AddDates( dStart, dEnd, nStart, nEnd ) CLASS SampleCalex

   local dDate
   local aGrad

   DEFAULT dStart := CToD( "  /  /  " )
   DEFAULT dEnd   := CToD( "  /  /  " )
   DEFAULT nStart := 0
   DEFAULT nEnd   := 0

   //Clear scopes
   ( ::cAlias )->( OrdSetFocus( "fecha" ) )
   ( ::cAlias )->( OrdScope( TOPSCOPE, NIL ) )
   ( ::cAlias )->( OrdScope( BOTTOMSCOPE, NIL ) )
   
   //Set New scopes
   ( ::cAlias )->( DBGoTop() )
   ( ::cAlias )->( OrdScope( TOPSCOPE,    DToS( dStart ) + Str( nStart * 100, 4 ) ) )
   ( ::cAlias )->( OrdScope( BOTTOMSCOPE, DToS( dEnd )   + Str( nEnd * 100, 4 ) ) )
   ( ::cAlias )->( DBGoTop() )
   ::oCalex:Reset()
   DO WHILE ! ( ::cAlias )->( Eof() )
      //nStart, nEnd, dStart, dEnd, cText, cSubject, nIdx, lND, lAplicado, lBloqueoT, aGrad, nClrT, nStatus
      aGrad   := ::ColorStatus( ( ::cAlias )->nStatus, ( ::cAlias )->DSTART )
      ::oCalex:LoadDates( ( ::cAlias )->START,;
                          ( ::cAlias )->END, ;
                          ( ::cAlias )->DSTART, ;
                          ( ::cAlias )->DEND, ;
                          ( ::cAlias )->TEXT, ;
                          ( ::cAlias )->SUBJECT,;
                          ( ::cAlias )->IDX, , , , ;
                          aGrad[ 1 ], ;
                          aGrad[ 2 ], ;
                          ( ::cAlias )->nStatus )
      ( ::cAlias )->( DBSkip() )
   ENDDO
   
RETURN nil

//---------------------------------------------------//

METHOD BuildCalex() CLASS SampleCalex
   
   local oSelf := Self

   if !Empty( ::oFld ) .and. ::lFld
   DEFINE CALEX ::oCalex OF ::oFld:aDialogs[ 1 ] ; //::oPanelCalex ;
          FROM 1, 10 TO 600*1.15, 600*1.78 ;
          FIRST_DATE 0 //ALL //TOP //LEFT //BOTTOM //RIGHT
   else
   DEFINE CALEX ::oCalex OF ::oPanelCalex ;
          FROM 1, 10 TO 600, 600 ;
          FIRST_DATE 0 ALL //TOP //LEFT //BOTTOM //RIGHT
   endif
   //::oCalex:bRClicked := { | r, c | MsgInfo( ::oCalex:GetInfoFromCoors( r, c ) ) }
   //::oCalex:bRClicked := { | r, c | MsgInfo( ::oCalex:hCalInfo[1] ) }
   //::oCalex:bRClicked := { | r, c | XBrowse( ::oCalex:hCalInfo ), MsgInfo( r ), MsgInfo( c ) }
   ::oCalex:bRClicked   := { | r, c | ::GetDateMonth( r, c ) }
   //::oCalex:bMouseWheel := { | nK, nD, c, r | 

   DEFINE MONTH VIEW OF ::oCalex;
          ON SELECT VIEW ( oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ), ;
                           oSelf:oDtPick:SetDate( ::dDateSelected )  ) ;
          ON SELECT DAY  oSelf:oDtPick:SetDate( dDate ) ;
          ON SELECT WEEK oSelf:oDtPick:SetDate( dDate ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ACTIVATE

   //Day

   DEFINE DAY VIEW OF ::oCalex ;
          INTERVAL ::oCalex:aOldInterval[ ::nOptSelex ] ; // 30 ;
          START HOUR 0 ;
          END HOUR 24;
          ON SELECT VIEW ( oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ), ;
                           oSelf:oDtPick:SetDate( ::dDateSelected )  ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON RIGHT CLICK ( oSelf:BuildPop( nRow, nCol, Self, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) );
          ON DELETE      ( oSelf:DeleteInfo( Self, nIdx ) )
                           
   
   //Week
   
   DEFINE WEEK VIEW OF ::oCalex ;
          INTERVAL ::oCalex:aOldInterval[ ::nOptSelex ] ; // 30 ;
          START HOUR 0 ;
          END HOUR 24;
          ON SELECT VIEW ( oSelf:oDtPick:SetDate( oLast:dDateSelected - DoW( oLast:dDateSelected ) + 1 ),;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) ) ;
          ON RIGHT CLICK ( oSelf:BuildPop( nRow, nCol, Self, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) ) ;
          ON NEXT        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON PREV        ( oSelf:oDtPick:SetDate( dDate ), ;
                           oSelf:AddDates( ::dStart, ::dEnd, ::nStartHour, ::nEndHour ) );
          ON DELETE      ( oSelf:DeleteInfo( Self, nIdx ) )
  
RETURN nil

//---------------------------------------------------//

METHOD BuildDbf() CLASS SampleCalex

   local aStructure
   local oSelf       := Self
   local lAppend     := .F.
   local i
   local nDay
   local nMonth
   local nYear
   local nStat
   local nTime1
   local nTime2
   local nOpc
   local aVals       := { "00", "05", "10", "15", "20", "30", "45" }
   local cLastOrd

   if ! File( ::cFile )
      aStructure = {  { "start"    , "N",   4, 0 },;
      	             { "end"      , "N",   4, 0 },;
      	             { "dstart"   , "D",   8, 0 },;
      	             { "subject"  , "C", 100, 0 },;
      	             { "text"     , "M",  10, 0 },;
      	             { "idx"      , "N",  10, 0 },;
      	             { "dend"     , "D",   8, 0 },;
                      { "lRemember", "L",   1, 0 },;
                      { "nMinutes",  "N",   4, 0 },;
                      { "nStatus",   "N",   2, 0 } }

      	             	
      DBCreate( oSelf:cFile, aStructure, "DBFCDX" )
      lAppend  := .T.
   ENDIF      	             	

   USE ( oSelf:cFile ) NEW ALIAS ( oSelf:cAlias ) VIA "DBFCDX"
   INDEX ON DToS( citas->dstart ) + StrZero( citas->start, 4 ) TAG fecha TO ( ::cAlias )
   INDEX ON citas->idx TAG idx TO ( ::cAlias )
   INDEX ON citas->nStatus TAG status TO ( ::cAlias )

#ifdef DEMO
   if lAppend
      cLastOrd := ( ::cAlias )->( OrdSetFocus( "fecha" ) )
      CursorWait()
      for i := 1 to 6000
         nMonth := StrZero( HB_RandomInt( 1, 12 ), 2 )
         nDay   := StrZero( HB_RandomInt( 1, if( nMonth != "02", 30, 28 ) ), 2 ) 
         nYear  := StrZero( HB_RandomInt( 2016, 2018 ), 4 )
         nTime1 := StrZero( HB_RandomInt( 5, 22 ), 2 )
         nTime2 := StrZero( HB_RandomInt( Val( nTime1 ), 23 ), 2 )
         nStat  := HB_RandomInt( 1, 10 )
         nOpc   := HB_RandomInt( 1,  7 )
         if !( ::cAlias )->( DbSeek( Dtos( Ctod( nMonth + "/" + nDay + "/" + nYear ) ) + nTime1 + aVals[ nOpc ] ) )
         ( ::cAlias )->( DbAppend() )
         REPLACE ( ::cAlias )->idx       WITH i
         REPLACE ( ::cAlias )->start     WITH Val( nTime1 + aVals[ nOpc ] )
         nOpc   := nOpc + 1
         nOpc   := if( nOpc > 7, 1, nOpc )
         REPLACE ( ::cAlias )->end       WITH Val( nTime2 + aVals[ nOpc ] )
         REPLACE ( ::cAlias )->dstart    WITH Ctod( nMonth + "/" + nDay + "/" + nYear )
         REPLACE ( ::cAlias )->dend      WITH Ctod( nMonth + "/" + nDay + "/" + nYear )
         REPLACE ( ::cAlias )->subject   WITH "Subject: " + nDay + "/" + nMonth
         REPLACE ( ::cAlias )->text      WITH "Text: " + nDay + "/" + nMonth + "/" + nYear
         REPLACE ( ::cAlias )->nStatus   WITH nStat
         REPLACE ( ::cAlias )->lRemember WITH .T.
         REPLACE ( ::cAlias )->nMinutes  WITH 5 * nStat
         endif
      next
      CursorArrow()
      ( ::cAlias )->( OrdSetFocus( cLastOrd ) )
      ( ::cAlias )->( DbGoTop() )
   endif
#endif

RETURN nil

//---------------------------------------------------//

METHOD ViewDatabase() CLASS SampleCalex

   local oSelf    := Self
   local nOldRec  := ( ::cAlias )->( RecNo() )
   local cLastOrd := ( ::cAlias )->( OrdSetFocus( "fecha" ) )

   ( ::cAlias )->( DBGoTop() )
   XBROWSER ::cAlias SETUP ( oBrw:lFooter := .F. ) SHOW SLNUM //AUTOSORT // OR SHOW RECID

   ( ::cAlias )->( OrdSetFocus( cLastOrd ) )
   ( ::cAlias )->( DBGoTo( nOldRec ) )

Return nil

//----------------------------------------------------------------------------//

METHOD BuildDialog( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) CLASS SampleCalex

   local oDlg
   local cSubject := Space( 100 )
   local cText
   local lSave    := .F.
   local lNew     := oView:oCalex:oCalInfoSelected == NIL
   local o, nID, cLastOrd
   
   DEFINE DIALOG oDlg TITLE "Adding Data" SIZE 300, 370
   
   if ! lNew
      o        := oView:oCalex:oCalInfoSelected
      cSubject := o:cSubject
      cText    := o:cText
   endif
   
   
   @ 10, 10 SAY "Date: " + DToC( dDateFrom ) OF oDlg PIXEL
   //@ 10, 10 SAY "Date: " + DToC( dDateTo ) OF oDlg PIXEL
   @ 25, 10 SAY "Time Start: [" + oView:ConvertTime( nTimeFrom, oView:lAmPm ) +;
                "] -- Time End: [" + oView:ConvertTime( nTimeTo, oView:lAmPm ) + "]" OF oDlg PIXEL
   @ 40, 10 SAY "Subject" OF oDlg PIXEL
   @ 40, 30 GET cSubject OF oDlg PIXEL SIZE 100, 10
   @ 55, 10 SAY "Text" OF oDlg PIXEL
   @ 70, 10 GET cText MEMO OF oDlg PIXEL SIZE 130, 95
   @ 170, 10 BUTTON "OK" PIXEL ACTION ( lSave := .T., oDlg:End() )
   @ 170, 100 BUTTON "CANCEL" PIXEL ACTION ( oDlg:End() )
   
   ACTIVATE DIALOG oDlg CENTERED

   IF lSave 
      cLastOrd = ( ::cAlias )->( OrdSetFocus( "idx" ) )
      if lNew         
         nID = ( ::cAlias )->( DBGoBottom() )
         nID = ( ::cAlias )->IDX + 1
         ( ::cAlias )->( DBAppend() )
         REPLACE ( ::cAlias )->IDX  WITH nID
      else 
         ( ::cAlias )->( DBSeek( o:nIdx ) )
         o:cSubject = cSubject
         o:cText    = cText            
      endif
      
      REPLACE ( ::cAlias )->start   WITH nTimeFrom
      REPLACE ( ::cAlias )->end     WITH nTimeTo
      REPLACE ( ::cAlias )->dstart  WITH dDateFrom
      //REPLACE ( ::cAlias )->dend    WITH dDateTo
      REPLACE ( ::cAlias )->subject WITH cSubject
      REPLACE ( ::cAlias )->text    WITH cText
      
      ( ::cAlias )->( DBCommitAll() )
      ::AddDates( oView:dStart, oView:dEnd, oView:nStartHour, oView:nEndHour )      
      oView:BuildDates()      
      ( ::cAlias )->( OrdSetFocus( cLastOrd ) )
      
   ENDIF

RETURN nil

//---------------------------------------------------//

METHOD BuildExplorer() CLASS SampleCalex

   local oPanel
   local oSelf := Self
   local nOption := 5
   
   ::oExBar := TExplorerBar():New( 0, 0, 250, 300, ::oPanelExplorer )
   
   oPanel := ::oExBar:AddPanel( Upper( "Jump To Date" ), "d:\fwh\bitmaps\32x32\calendar.bmp", 200 )
   @ 40, 15 CALENDAR ::oDtPick VAR ::dDateStart OF oPanel PIXEL SIZE 220, 157 WEEKNUMBER DAYSTATE
              
   ::oDtPick:bChange = { | o | ::ChangeDate( o ) }
   
   oPanel := ::oExBar:AddPanel( Upper( "View" ), "c:\fwh\bitmaps\32x32\view.bmp", 130 )
   oPanel:AddLink( "View Daily"  , { || ::oCalex:SetDayView(), ::ChangeSelex()  }, "d:\fwh\bitmaps\16x16\inspect.bmp" )
   oPanel:AddLink( "View Weekly" , { || ::oCalex:SetWeekView(), ::ChangeSelex() }, "d:\fwh\bitmaps\16x16\additem.bmp" )
   oPanel:AddLink( "View Monthly", { || ::oCalex:SetMonthView() }, "c:\fwh\bitmaps\16x16\calendar.bmp" )

   @ 100, 10 SELEX ::oSelex VAR nOption of oPanel PIXEL SIZE 220, 45;
      ITEMS "5", "10", "15", "20", "30", "60" ;
      GRADIENT OUTTRACK { { 1/2, nRGB( 219, 230, 244 ), nRGB( 207-50, 221-25, 255 ) }, ;
                          { 1/2, nRGB( 201-50, 217-25, 255 ), nRGB( 231, 242, 255 ) } }; 
      LINECOLORS nRGB( 237, 242, 248 ), nRGB( 141, 178, 227 );
      COLORTEXT  CLR_BLUE, CLR_RED ;
      ACTION (::oCalex:oView:SetInterval( Val( ::oSelex:aOptions[ nOption ] ) ), ::oCalex:Refresh() )

   oPanel := ::oExBar:AddPanel( Upper( "Actions" ), "d:\fwh\bitmaps\32x32\index2.bmp", 140 )
   oPanel:AddLink( "New record",  { || msginfo("New")  }, "d:\fwh\bitmaps\16x16\adddbf.bmp" )
   oPanel:AddLink( "Modify record", { || msginfo("Modify")  }, "d:\fwh\bitmaps\16x16\edit.bmp" )
   oPanel:AddLink( "Delete record",  { || msginfo("Delete")  }, "d:\fwh\bitmaps\16x16\deldbf.bmp" )
   //oPanel:AddLink( "Copy record",  { || msginfo("Copy")  }, "d:\fwh\bitmaps\16x16\copy.bmp" )
   oPanel:AddLink( "Informes",  { || oSelf:Imprime()  }, "d:\fwh\bitmaps\16x16\print.bmp" )
   oPanel:AddLink( "View Database",  { || oSelf:ViewDatabase() }, "d:\fwh\bitmaps\16x16\prg.bmp" )
   oPanel:AddLink( "Exit",  { || oSelf:oWnd:End()  }, "d:\fwh\bitmaps\16x16\exit2.bmp" )

   oPanel := ::oExBar:AddPanel( Upper( "INFO" ), "d:\fwh\bitmaps\32x32\notes.bmp", 52 )
   oPanel:AddLink( "Info",  { || XBrowse( ::oCalex:hCalInfo ) }, "d:\fwh\bitmaps\16x16\prg.bmp" )

   ::oPanelExplorer:oClient := ::oExBar
   ::oPanelExplorer:oClient:bMouseWheel := { || MsgInfo( "ok" ) }

Return nil

//---------------------------------------------------//

METHOD BuildPanels() CLASS SampleCalex

   local oBrush
   local oParent

   if ::oWnd:ClassName() == "TMDICHILD"
      oParent  := ::oWnd
   else
      oParent  := ::oWnd:oWndClient
   endif
   ::oPanelExplorer := TPanel():New( 1, 0, ::oWnd:nHeight, 280, oParent )
   ::oPanelCalex    := TPanel():New( 1, ::oPanelExplorer:nWidth + 6, ::oWnd:nHeight, ;
                                     ::oWnd:nWidth - ::oPanelExplorer:nWidth + 6, oParent )

   DEFINE BRUSH oBrush COLOR CLR_WHITE
   ::oPanelExplorer:SetBrush( oBrush )
   ::oPanelCalex:SetBrush( oBrush )
   oBrush:End()
   
   if ::lFld
   @ 1, 1 FOLDEREX ::oFld ;
         PROMPTS "Calex" OF ::oPanelCalex ;
         TAB HEIGHT 22 ;
         SIZE  2 *::oPanelCalex:nWidth - 70, 2* ::oPanelCalex:nHeight - 266 ; 
         PIXEL //FONT oThis:oFontXF
   endif

Return nil

//---------------------------------------------------//

METHOD ChangeSelex() CLASS SampleCalex
   
   local cInterval := AllTrim( Str( ::oCalex:oView:nInterval ) )
   local nAt       := AScan( ::oSelex:aOptions, cInterval )

   if nAt != 0  .and. ::oSelex:nOption != nAt
      ::oSelex:SetOption( nAt )
      ::oSelex:Refresh()
   endif 

Return nil

//---------------------------------------------------//

METHOD SetSize( nType, nWidth, nHeight ) CLASS SampleCalex

   if nWidth != nil 
      ::oPanelExplorer:Move( , , , nHeight )
      ::oPanelCalex:Move( , , nWidth - ( ::oPanelExplorer:nRight + 6 ), nHeight )
   endif
   
Return nil

//---------------------------------------------------//

METHOD GetDateMonth( r, c, lView ) CLASS SampleCalex

   local dDate
   local aPos      := {}
   local oCalInfo
   local cStrDay
   local aInfo
   local hDays
   local aDates    := {}
   local nH
   local nW
   DEFAULT lView   := .T.

   if ::oCalex:oView:ClassName() == Upper( "TMonthView" )
      aPos   := ::oCalex:oView:GetPosition( r, c )
      dDate  := ::oCalex:oView:GetDateFromPos( aPos[ 1 ], aPos[ 2 ] ) 
      //XBrowse( ::oCalex:oView:GetCoorFromPos( aPos[ 1 ], aPos[ 2 ] ) )
      cStrDay := DToS( dDate )
      hDays = hb_HASH()
      for each oCalInfo in ::oCalex:hCalInfo
         aInfo = {}
#ifdef __XHARBOUR__
         oCalInfo := oCalInfo:Value
#endif
         if oCalInfo:dStart == dDate
            if hb_HHASKEY( hDays, cStrDay )
               aInfo := hb_HGET( hDays, cStrDay )
            endif
            AAdd( aInfo, oCalInfo )
            ASort( aInfo, , ,{ | o1, o2 | o1:nStart < o2:nStart } )
            //AAdd( aDates, { oCalInfo:nIdx, oCalInfo:dStart, oCalInfo:dEnd, oCalInfo:nStart, oCalInfo:nEnd, RTrim( oCalinfo:cSubject ), oCalInfo:nStatus } )
            AAdd( aDates, { oCalInfo:nStart, oCalInfo:nStart + oCalInfo:nEnd, RTrim( oCalinfo:cSubject ), oCalInfo:nStatus, oCalInfo:nIdx } )
            //XBrowse( aInfo )
            hb_HSET( hDays, cStrDay, aInfo )
         endif
      next
   endif

   if lView .and. !Empty( aDates )
      //XBrowse( aDates ) //hDays )
      //XBrowse( ::oCalex:oCalInfo:aCoords )
      if !Empty( ::oCalex:oCalInfo )
      //r  := ::oCalex:oCalInfo:aCoords[ 1 ]
      //c  := ::oCalex:oCalInfo:aCoords[ 2 ]
      
      //nH := Int( ::oCalex:oCalInfo:aCoords[ 3 ] / 2 ) - 1
      //nW := Int( ::oCalex:oCalInfo:aCoords[ 4 ] / 2 ) + 1
      nH := ::oCalex:oView:nRowHeight - 21
      nW := ::oCalex:oView:nColWidth  - 3

      APopupBrowse( aDates, ::oCalex:oWnd, r - 1, c - 9, nH, nW, ;
          { | o | ( o:lHeader := .F. ) }, 5 )
          //{ | o | ( o:cHeaders := { "HourS", "HourE", "Subject", "St", "Id" } ) }, 1 )
          //{ | o | ( o:cHeaders := { "Id", "Start", "End", "HourS", "HourE", "Subject", "St" } ) }, 1 )
      endif
   endif

Return dDate

//----------------------------------------------------------------------------//

METHOD BuildPop( nRow, nCol, oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo ) CLASS SampleCalex

   local oMenu
   local oSelf    := Self
   local lNew     := oView:oCalex:oCalInfoSelected == NIL
   local oCalex   := oView:oCalex

   MENU oMenu POPUP
      MENUITEM If( lNew, "New appointment", "Modify appointment" ) ;
         ACTION oSelf:BuildDialog( oView, dDateFrom, dDateTo, nTimeFrom, nTimeTo )
      SEPARATOR
      if ! lNew                         
         MENUITEM "Delete appointment"  ACTION If( MsgYesNo( "Are you sure?" ), oView:oCalex:DelCalInfo(), )
      endif
      
      MENUITEM "Today" ACTION ( oView:SetDate( date() ), If( oView:IsKindOf( "TWEEKVIEW" ),;
                                 oView:oCalex:SetWeekView(), oView:oCalex:SetDayView() ) )
      SEPARATOR

      MENUITEM  oItems[ 1 ] PROMPT "5  minutes" CHECKED ACTION ( oSelf:ChangeView( oView,  5 ) )
      MENUITEM  oItems[ 2 ] PROMPT "10 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 10 ) )
      MENUITEM  oItems[ 3 ] PROMPT "15 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 15 ) )
      MENUITEM  oItems[ 4 ] PROMPT "20 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 20 ) )
      MENUITEM  oItems[ 5 ] PROMPT "30 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 30 ) )
      MENUITEM  oItems[ 6 ] PROMPT "60 minutes" CHECKED ACTION ( oSelf:ChangeView( oView, 60 ) )

   ENDMENU
   ::ChangeCheck()

   ACTIVATE POPUP oMenu OF oView:oCalex AT nRow, nCol

Return nil	

//----------------------------------------------------------------------------//

METHOD ChangeView( oView, nInterval ) CLASS SampleCalex

   local oCalex   := oView:oCalex
   local nPos     := AsCan( ::oCalex:aOldInterval, nInterval )

   if !Empty( nPos )
      oCalex:oWeekView:SetInterval( nInterval )
      oCalex:oWeekView:Refresh()
      oCalex:oDayView:SetInterval( nInterval )
      oCalex:oDayView:Refresh()
      ::ChangeCheck( nPos )
   endif

Return nil	

//----------------------------------------------------------------------------//

METHOD ChangeCheck( nPos ) CLASS SampleCalex

   local x
   DEFAULT nPos  := 0

   if !Empty( nPos )
      aChecks[ nPos ] := ! aChecks[ nPos ]
      For x = 1 to Len( oItems )
         if nPos != x
            if aChecks[ x ]
               aChecks[ x ] := ! aChecks[ x ]
            endif
         else
            if ! aChecks[ x ]
               aChecks[ x ] := ! aChecks[ x ]
            endif
         endif
         oItems[ x ]:SetCheck( aChecks[ x ] )
      Next x
      if !Empty( ::oSelex )
         ::ChangeSelex()
      endif
   else
      For x = 1 to Len( oItems )
          oItems[ x ]:SetCheck( aChecks[ x ] )
      Next x
   endif
Return nil

//---------------------------------------------------//

METHOD DeleteInfo( oView, nIdx ) CLASS SampleCalex

   local cLastOrd  := ( ::cAlias )->( OrdSetFocus( "idx" ) )

   ( ::cAlias )->( DBSeek( nIdx ) )
   ( ::cAlias )->( DBDelete() )
   ( ::cAlias )->( OrdSetFocus( cLastOrd ) )

RETURN NIL

//---------------------------------------------------//

METHOD ChangeDate( oDatePick ) CLASS SampleCalex

   ::oCalex:oView:SetDate( oDatePick:GetDate() )
   
   if ::oCalex:oView:IsKindOf( "TMONTHVIEW" )
      ::oCalex:SetMonthView()
   elseif ::oCalex:oView:IsKindOf( "TWEEKVIEW" )
      ::oCalex:SetWeekView()
   else
      ::oCalex:SetDayView()
   endif
   
RETURN NIL

//----------------------------------------------------------------------------//

METHOD ColorStatus( nType, dFecha ) CLASS SampleCalex

   local aGrad
   local nClrT     := CLR_BLACK
   DEFAULT nType   := 0

   DO CASE
      case nType = 1
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }
         nClrT     := CLR_BLACK
      case nType = 2
         aGrad := { { 1, RGB(0x00,0x80,0xff) , RGB(0x00,0x80,0xff) } }
         nClrT     := CLR_BLACK
      case nType = 3
         aGrad := { { 1, RGB(0xff,0xff,0x80), RGB(0xff,0xff,0x80) } }
         nClrT     := CLR_BLACK
      case nType = 4
         aGrad := { { 1, RGB(0xff,0x00,0x00), RGB(0xff,0x00,0x00) } }
         nClrT := CLR_WHITE
      case nType = 5
         aGrad := { { 1, RGB(0xc0,0xc0,0xc0), RGB(0xc0,0xc0,0xc0) } }
         nClrT     := CLR_BLACK
      case nType = 6
         aGrad := { { 1, METRO_ORANGE, METRO_ORANGE } }
         nClrT     := CLR_BLACK
      case nType = 7
         aGrad := { { 1, METRO_LIME, METRO_LIME } }
         nClrT     := CLR_BLACK
      case nType = 8
         aGrad := { { 1, METRO_CRIMSON, METRO_CRIMSON } }
         nClrT     := CLR_BLACK
      case nType = 9
         aGrad := { { 1, METRO_OLIVE, METRO_OLIVE } }
         nClrT     := CLR_BLACK
      case nType = 10
         aGrad := { { 1, METRO_STEEL, METRO_STEEL } }
         nClrT     := CLR_BLUE
      Otherwise
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }      
  ENDCASE

return { aGrad, nClrT }

//----------------------------------------------------------------------------//

METHOD Imprime( dFecha ) CLASS SampleCalex

   local oRep
   local oFont1, oFont2, acor, oDlg1, oGet1, oGet2, oGet3, oBot1, oBot2
   local lRep    := .f.
   local dDesde
   local dHasta
   local mestado := 1
   
   DEFAULT dFecha := ::oCalex:dDateSelected
   
   dDesde := dFecha
   dHasta := dFecha

   // Defino los distintos tipos de letra
   DEFINE FONT oFont1 NAME "ARIAL" SIZE 0,-10
   DEFINE FONT oFont2 NAME "ARIAL" SIZE 0,-10 BOLD
   
   
   DEFINE DIALOG oDlg1 TITLE "Informe de Citas" FROM 03,15 TO 13,70 //       OF oApp:oWnd
   
   @ 07, 05 SAY "Estado:"               OF oDlg1 PIXEL SIZE 50,12 RIGHT
   @ 22, 05 SAY "Desde Fecha:"          OF oDlg1 PIXEL SIZE 50,12 RIGHT
   @ 37, 05 SAY "Hasta Fecha:"          OF oDlg1 PIXEL SIZE 50,12 RIGHT
   
   @ 05, 60 COMBOBOX oGet1 VAR mestado OF oDlg1 SIZE 60,12 PIXEL ;
      ITEMS { "Todas", "Solo pendientes", "Solo Realizadas" }
   @ 20, 60 GET oGet2 VAR dDesde  OF oDlg1 PIXEL
   @ 35, 60 GET oGet3 VAR dHasta  OF oDlg1 PIXEL VALID ( dHasta >= dDesde )
   @ 50,40 BUTTON oBot1 PROMPT "&Imprimir" OF oDlg1 SIZE 30,10 ;
           ACTION ( lRep := .t., oDlg1:End() ) PIXEL
   @ 50,90 BUTTON oBot2 PROMPT "&Cancelar" OF oDlg1 SIZE 30,10 ;
           ACTION ( lRep := .f., oDlg1:End() ) PIXEL
   ACTIVATE DIALOG oDlg1 CENTER
   
   IF !lRep
      RETURN nil
   ENDIF
   
   REPORT oRep ;
       TITLE "Citas de " + ALLTRIM( "TCalex" )  + " del " + DTOC( dDesde ) + " al " + DTOC( dHasta ) LEFT ;
       FONT  oFont1, oFont2 ;
       HEADER OemToAnsi( "Sample01" ) RIGHT ;
       FOOTER "Hoja: " + STR( oRep:nPage, 3 ) ,"Fecha: " + DToc( DATE() ) LEFT ;
       PREVIEW CAPTION "Citas"
   COLUMN TITLE "Id"      DATA ( ::cAlias )->idx     SIZE 07 FONT 1
   COLUMN TITLE "Subject" DATA Left( ( ::cAlias )->subject, 40 ) SIZE 20 FONT 1
   COLUMN TITLE "Fecha"   DATA ( ::cAlias )->dStart SIZE 08 FONT 2
   COLUMN TITLE "Desde"   DATA TRANSFORM( ( ::cAlias )->start, "999:99" ) SIZE 05 FONT 1 RIGHT
   COLUMN TITLE "Hasta"   DATA TRANSFORM( ( ::cAlias )->end,   "999:99" ) SIZE 05 FONT 1 RIGHT
   COLUMN TITLE "Asunto"  DATA ( ::cAlias )->text    SIZE 25 FONT 1 MEMO
   //COLUMN TITLE "Motivo"  DATA ( ::cAlias )->motivo  SIZE 20 FONT 1
   //COLUMN TITLE "Alarma"  DATA IF( ( ::cAlias )->lRemember, "SI", "NO" ) SIZE 05 FONT 1
   //COLUMN TITLE "Estado"  DATA IF( ( ::cAlias )->nStatus,"REALIZADA  ","PENDIENTE") SIZE 10 FONT 1

   // Digo que el titulo lo escriba con al letra 2
   oRep:oTitle:aFont[1] := {|| 2 }
   oRep:oTitle:aFont[1] := {|| 2 }
   oRep:bInit           := {|| ( ::cAlias )->( DbGoTop() ) }
   //oRep:bSkip := {|| ( ::cAlias )->Skip() }

   END REPORT

   ACTIVATE REPORT oRep //WHILE ( ::cAlias )->EOF() //;
      //ON INIT CursorArrow() ;
      //ON STARTPAGE oRep:SayBitmap( .1, .1, "LOGO.BMP", .5, .5 )

   RELEASE oFont1
   RELEASE oFont2 

RETURN nil

//----------------------------------------------------------------------------//

function APopupBrowse( aValue, oWnd, nRow, nCol, nH, nW, bInit, nColumn )

   local oDlg
   local oBrw
   local bGetValid
   local uOrgl
   local uRet
   local aPoint    := { nRow, nCol }
   local cFace     := "Calibri"
   local nHFont    := -10
   local oFont

   DEFAULT oWnd    := GetWndDefault()
   DEFAULT nW      := 500
   DEFAULT nH      := 180
   DEFAULT nColumn := 1

   if !Empty( aValue )
      aPoint = ClientToScreen( oWnd:hWnd, aPoint )
      DEFINE FONT oFont        NAME cFace SIZE 0, nHFont
      DEFINE DIALOG oDlg OF oWnd STYLE WS_POPUP ;
         SIZE nW, nH
      
      ACTIVATE DIALOG oDlg NOWAIT ;
         ON INIT oDlg:SetPos( aPoint[ 1 ], aPoint[ 2 ] ) ;
         VALID ( oFont:End(), .T. )
      
      @ 0, 0 XBROWSE oBrw DATASOURCE aValue AUTOCOLS AUTOSORT ;
         SIZE oDlg:nWidth, oDlg:nHeight OF oDlg PIXEL NOBORDER FONT oFont
      
      WITH OBJECT oBrw
         :lRecordSelector  := .F.
         :lHScroll         := .F.
         :lFlatStyle       := .T.
         :lFastDraw        := .T.
         :AutoFit()
         :bClrStd    := { || { ColorStatus( oBrw:aRow[ 4 ] )[ 2 ], ColorStatus( oBrw:aRow[ 4 ] )[ 1 ] } }
      
         if bInit != nil
            Eval( bInit, oBrw )
         endif
         :CreateFromCode()
         
         :PostMsg( WM_SETFOCUS )
         :bKeyDown   := { | nKey | If( nKey == VK_RETURN, ( Eval( oBrw:bChange ), oDlg:End() ), ;
                                   If( nKey == VK_ESCAPE, oDlg:End(), ) ) }
         :bLDblClick := { | r, c | ( oDlg:End() ) }
         :bChange    := { || uRet := oBrw:aCols[ nColumn ]:Value }
         :bLButtonUp := { | nRow, nCol | Eval( oBrw:bChange ) }
         //:Seek( oGet:GetText() )
      END
   endif

return uRet

//----------------------------------------------------------------------------//

Function ColorStatus( nType, dFecha )

   local aGrad
   local nClrT     := CLR_BLACK
   DEFAULT nType   := 0

   DO CASE
      case nType = 1
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }
         nClrT     := CLR_BLACK
      case nType = 2
         aGrad := { { 1, RGB(0x00,0x80,0xff) , RGB(0x00,0x80,0xff) } }
         nClrT     := CLR_BLACK
      case nType = 3
         aGrad := { { 1, RGB(0xff,0xff,0x80), RGB(0xff,0xff,0x80) } }
         nClrT     := CLR_BLACK
      case nType = 4
         aGrad := { { 1, RGB(0xff,0x00,0x00), RGB(0xff,0x00,0x00) } }
         nClrT := CLR_WHITE
      case nType = 5
         aGrad := { { 1, RGB(0xc0,0xc0,0xc0), RGB(0xc0,0xc0,0xc0) } }
         nClrT     := CLR_BLACK
      case nType = 6
         aGrad := { { 1, METRO_ORANGE, METRO_ORANGE } }
         nClrT     := CLR_BLACK
      case nType = 7
         aGrad := { { 1, METRO_LIME, METRO_LIME } }
         nClrT     := CLR_BLACK
      case nType = 8
         aGrad := { { 1, METRO_CRIMSON, METRO_CRIMSON } }
         nClrT     := CLR_BLACK
      case nType = 9
         aGrad := { { 1, METRO_OLIVE, METRO_OLIVE } }
         nClrT     := CLR_BLACK
      case nType = 10
         aGrad := { { 1, METRO_STEEL, METRO_STEEL } }
         nClrT     := CLR_BLUE
      Otherwise
         aGrad := { { 1, RGB(0x9a,0xcd,0x32), RGB(0x9a,0xcd,0x32) } }      
  ENDCASE

return { aGrad, nClrT }

//----------------------------------------------------------------------------//
