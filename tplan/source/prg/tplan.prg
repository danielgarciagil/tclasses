//TPlanning.prg

#include "fivewin.ch"
#include "planning.ch"

#ifdef __XHARBOUR__
#include "hbcompat.ch"
#endif

#define DEFAULT_GUI_FONT    17
#define SM_CXVSCROLL         2



#define TME_LEAVE           2
#define WM_MOUSELEAVE     675

#define PLANN_MONTH   1
#define PLANN_WEEK    2
#define PLANN_DAY     3
#define PLANN_CUSTOM  4

#define DT_CENTER     1
#define DT_VCENTER    4
#define DT_SINGLELINE 32

#define PLANN_ROW_HASH    1
#define PLANN_ROW_TEXTO   2

#define PLANN_HT_BODY     1
#define PLANN_HT_HEADER   2
#define PLANN_HT_NONE     3

#define PLANN_ATROW       1
#define PLANN_ATCOL       2


static nID := 0

CLASS TPlanning FROM TControl 
 
   CLASSDATA lRegistered AS LOGICAL 

   //ARRAYS
   DATA aLabelHeader
   DATA aGradHeaderCel
   DATA aGradLabel
   DATA aSelected

   //CODEBLOCK
   DATA bClrLabelHeader
   DATA bClrData
   DATA bClrTextData
   DATA bRSelected
   DATA bOnCapture
   DATA bOnResizedData

   //CHARACTER
   DATA cHeader

   //DATES
   DATA dStart, dEnd, dDate AS DATE

   // HASH
   DATA hRows
   
   //HANDLES
   DATA hConRight
   DATA hConLeft
   DATA pHitTest

   //LOGICAL
   DATA lSBVisible
   DATA lVertGrad
   DATA lCaptured                     //check if a empty cell is captured
   DATA lNoHalfDay
   DATA lCatched                      

   //NUMERIC
   DATA nColorGrid                    // Grid line color
   DATA nColorGrid2                   // Grid internal line color
   DATA nColorCellSelected            // Color of border in cell selected
   DATA nDNameHeight                  // Day Name header size
   DATA nLeftLabelWidth               // Left label width size
   DATA nLeftMargin                   // Left Margin
   DATA nRightMargin                  // Right Margin
   DATA nTopMargin                    // Top Margin
   DATA nVirtualHeight
   DATA nVirtualTop
   DATA nTypePlann                    // Planning type
                                      // 1 Month
                                      // 2 week
                                      // 3 day
                                      // 4 custom
                                      // 2 = MONDAY
   DATA nRowHeight
   DATA nHeaderHeight   
   DATA nVScroll
   DATA nColDown, nRowDown, nLastColDown
   
   //OBJECTS
   DATA oCursorCatch
   DATA oHeaderFont
   DATA oDataFont
   DATA oLastData, oDataOver
   DATA oHeaderFont_aux   HIDDEN
   DATA oFont_aux         HIDDEN
   
   DATA nDaysOffset, nRoomsOffset


   METHOD New( oWnd, nClrText ) CONSTRUCTOR
   
   
   METHOD AddRow( cName, cTexto )
   METHOD AddData( cRow, dStart, dEnd, cDescribe )
   
   METHOD CheckScroll()
   
   METHOD ColWidth()        INLINE Int( ( ::GridWidth() - ::nLeftLabelWidth ) / Len( ::aLabelHeader ) )
   
   METHOD EraseBkGnd( hDC ) INLINE 0 
   
   METHOD DataDropAvailable( uRowId, dFecha1, dFecha2 )

   METHOD DeleteData( oData )
   METHOD Display() INLINE ::BeginPaint(), ::Paint(), ::EndPaint(), 0 
   METHOD Destroy() 
  
   METHOD FirstVisibleRow() INLINE Int( ::nVirtualTop / ::nRowHeight ) + 1

   METHOD GetAtRow( nRow )
   METHOD GetAtCol( nCol ) 
   METHOD GetData( nAtRow, nCol )
   METHOD GetRow( nAtRow )
   METHOD GetCol( nAtCol )
   METHOD GetCoorFromPos( nAtRow, nAtCol )
   METHOD GetLastRow()   INLINE Len( ::hRows ) * ::nRowHeight + ::nTopMargin + ::nHeaderHeight
   METHOD GetRowText()
   METHOD GetRowID()
   METHOD GridWidth()    INLINE ::nWidth - ::nRightMargin - ::nLeftMargin - ::nVScroll
   METHOD GridHeight()   INLINE ::nHeight - 4 - ::nTopMargin
   
   METHOD HandleEvent()

   METHOD HitTest( nRow, nCol ) 

   METHOD LButtonDown( nRow, nCol )
   METHOD LButtonUp( nRow, nCol ) 
   
   METHOD LastVisibleRow()  INLINE  Min( Len( ::hRows ), ::FirstVisibleRow() + ::TotalVisibleRow() )
   METHOD Line( hDC, nTop, nLeft, nBottom, nRight, nColor )

   METHOD ModCol()          INLINE ( ::GridWidth() - ::nLeftLabelWidth ) % Len( ::aLabelHeader )
   METHOD MouseLeave()
   METHOD MouseMove( nRow, nCol, nFlags ) 
   METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos )

   METHOD Paint() 
   METHOD PaintData( hDC )
   METHOD PaintDates( hDC )
   
   
   METHOD RButtonUp( nRow, nCol, nKeyFlags )
   METHOD Resize( nType, nWidth, nHeight ) INLINE ::CheckScroll(), ::Super:Resize( nType, nWidth, nHeight )
   METHOD Reset()
   
   METHOD SelectCell( )
   METHOD SetDates( dStart, dEnd )
   METHOD SetLabelHeader()
   METHOD SetScroll()
   
   METHOD ToTalVisibleRow() INLINE ( ::GridHeight() / ::nRowHeight ) + 1
   
   METHOD UpdateData( cRow, dStart, dEnd, cDescribe, cToolTip )
   
   METHOD VScrollSetPos( nPos )
   METHOD VScrollSkip( nSkip )

ENDCLASS 

//----------------------------------------------------------------------------// 

METHOD New( oWnd, oFont, oDataFont, oHeaderFont, dStart, dEnd, cHeader, lNoHalfDay ) CLASS TPlanning 

   local nMod, aFontInfo, n

   DEFAULT oWnd     := GetWndDefault() 
   DEFAULT cHeader  := "Rooms/Days"
   DEFAULT lNoHalfDay := .T.

   ::nTop       = 0
   ::nLeft      = 0
   ::nBottom    = 600
   ::nRight     = 800
   ::oWnd       = oWnd
   ::lNoHalfDay = lNoHalfDay

   ::nClrText    = 0
   ::nStyle      = nOr( WS_CHILD, WS_VISIBLE, WS_TABSTOP, WS_BORDER, WS_CLIPCHILDREN ) 


   //Array 
   ::aGradHeaderCel     = { { 1, nRGB( 165, 191, 225 ), nRGB( 165, 191, 225 ) } }
   ::aGradLabel         = { { 1, nRGB( 165, 191, 225 ), nRGB( 165, 191, 225 ) } }
   ::aSelected          = {}

   //Numeric
   ::nColorGrid       = nRGB( 141, 174, 217 )
   ::nColorGrid2      = nRGB( 230, 237, 247 )
   ::nColorCellSelected = nRGB(0xe6,0xe6,0xfa)   
   ::nLeftLabelWidth  = 100
   ::nLeftMargin      = 2
   ::nRightMargin     = 2
   ::nTopMargin       = 60
   ::nVirtualHeight   = ::nHeight
   ::nRowHeight       = 24
   ::nHeaderHeight    = 30
   ::nVirtualTop      = 0
   ::nVScroll         = 0
   ::nColDown         = 0
   ::nRowDown         = 0
   ::nLastColDown     = 0
   
   ::nDaysOffset      = 0
   ::nRoomsOffset     = 0
   
   //CodeBlocks 
   
   ::bClrLabelHeader = {|| ::nClrText }
   ::bClrData        = {|| { { 1, nRGB( 225, 234, 247 ), nRGB( 181, 202, 230 ) } } }
   ::bClrTextData    = {|| ::nClrText }
   
   //Character
   ::cHeader         = cHeader     
   
   //Hash 
   ::hRows := {=>}
   
   //Handles 
   ::hConLeft  = HConLeft()
   ::hConRight = HConRight()
   
   ::lVertGrad = .F.
   ::lCaptured = .F.
   
   if ::oFont != NIL
      ::oFont:End()
   endif
   
   if oFont != NIl 
      aFontInfo = GetFontInfo( oFont:hFont )
   else 
      aFontInfo = GetFontInfo( GetStockObject( DEFAULT_GUI_FONT ) ) 
   endif
   
   DEFINE FONT ::oFont NAME aFontInfo[ 4 ] SIZE aFontInfo[ 2 ], aFontInfo[ 1 ]
   DEFINE FONT ::oFont_aux NAME aFontInfo[ 4 ] SIZE aFontInfo[ 2 ], aFontInfo[ 1 ] BOLD

   if oDataFont != NIl 
      aFontInfo = GetFontInfo( oDataFont:hFont )
   else 
      aFontInfo = GetFontInfo( GetStockObject( DEFAULT_GUI_FONT ) ) 
   endif   
   
   DEFINE FONT ::oDataFont NAME aFontInfo[ 4 ] SIZE aFontInfo[ 2 ], aFontInfo[ 1 ]

   if oHeaderFont != NIL 
      aFontInfo = GetFontInfo( oHeaderFont:hFont )
   else 
      aFontInfo = GetFontInfo( GetStockObject( DEFAULT_GUI_FONT ) ) 
   endif   
   
   DEFINE FONT ::oHeaderFont NAME aFontInfo[ 4 ] SIZE aFontInfo[ 2 ] - 1, aFontInfo[ 1 ] - 1
   DEFINE FONT ::oHeaderFont_aux NAME aFontInfo[ 4 ] SIZE aFontInfo[ 2 ] - 1, aFontInfo[ 1 ] - 1 BOLD
   
   
   #ifdef __XPP__ 
      DEFAULT ::lRegistered := .F. 
   #endif 

  ::SetBrush( ::oWnd:oBrush )

   ::Register() 

   if ! Empty( oWnd:hWnd ) 
      ::Create() 
      oWnd:AddControl( Self ) 
   else 
      oWnd:DefControl( Self ) 
   endif 

   DEFINE SCROLLBAR ::oVScroll VERTICAL OF Self
   ::SetScroll()
   ::lSBVisible = .F.
   
   ::nTypePlann = PLANN_MONTH
   ::dDate = Date()
   
   DEFAULT dStart := SToD( StrZero( Year( ::dDate ), 4 ) + StrZero( Month( ::dDate ), 2 ) + "01" )
   DEFAULT dEnd   := P_GetLastDayMonth( ::dDate )    
   
   ::dStart = dStart
   ::dEnd   = dEnd
   ::SetLabelHeader()


return Self 

//----------------------------------------------------------------------------// 

METHOD AddRow( cName, cTexto ) CLASS TPlanning

   if cName != NIL .and. ValType( cName ) == "C"
      if cTexto == NIL
         cTexto = StrTran( cName, " ", "" )
      endif
      hb_HSET( ::hRows, cName, { {=>}, cTexto } )
   endif

return NIL

//----------------------------------------------------------------------------// 

METHOD AddData( cRow, dStart, dEnd, cDescribe, cToolTip ) CLASS TPlanning

   local oData
   local hDatas
   local aDatas
   
   if dEnd < ::dStart
      return nil 
   endif
   
   if cRow != NIL 
      if ValType( cRow ) == "C"
         if ! hb_HHASKEY( ::hRows, cRow )   
            ::AddRow( cRow )
         endif
         aDatas = hb_HGET( ::hRows, cRow )
         hDatas  = aDatas[ PLANN_ROW_HASH ]
         oData  = TPData():New( Self, cRow, dStart, dEnd, cDescribe, cToolTip )
         hb_HSET( hDatas, oData:cName, oData )
         aDatas[ PLANN_ROW_HASH ] = hDatas
         hb_HSET( ::hRows, cRow, aDatas )
      endif 
   endif

return oData
                
//----------------------------------------------------------------------------// 

METHOD CheckScroll() CLASS TPlanning

   local nLastRow
   local nPos
   
   if ! ::lActive
      RETURN nil
   endif
   
   nLastRow := ::GetLastRow()
   if nLastRow > ::nHeight
      ::nVirtualHeight = nLastRow
      SetScrollRangeX( ::hWnd, 1, 0, ::nVirtualHeight - 1) 

      if  (::nVirtualHeight - ::nVirtualTop) < ::nHeight
         ::nVirtualTop := ::nVirtualHeight - ::nHeight
      endif   

      ::oVScroll:SetPage( ::nHeight, .F. )
      ::oVScroll:SetPos( ::nVirtualTop )
      ::lSBVisible = .T.
      ::nVScroll = GetSysMetrics( SM_CXVSCROLL )

   else 
      ::nVirtualTop = 0
      ::nVirtualHeight = ::nHeight
      SetScrollRangeX( ::hWnd, 1, 0, 0 )
      ::lSBVisible = .F.
      ::nVScroll = 0
   endif

   ::SetFocus()
      
RETURN nil       

//----------------------------------------------------------------------------// 

//USED INTERNALLY ONLY
METHOD DataDropAvailable( uRowId, oData ) CLASS TPlanning

   local lOk := .T.
   local hDatas, oItem
   local n, days, bits
   local n2, days2, bits2

   if ( lOk := ! Empty( uRowId ) )
      hDatas = hb_HGET( ::hRows, uRowId )[ PLANN_ROW_HASH ]
      for each oItem in hDatas
   #ifdef __XHARBOUR__
         oItem = oItem:Value
   #endif
         if oItem:nId != oData:nId
            n = max(0, min(oItem:nColEnd,oData:nNewColEnd) - max(oItem:nColStart,oData:nNewColStart))
            lOk = lOk .AND. (n==0)
         endif
         if ! lOk
            exit
         endif
      next
   endif 
return lOk          

                
//----------------------------------------------------------------------------// 

METHOD DeleteData( oData, lRefresh ) CLASS TPlanning
   
   local hData
   
   DEFAULT lRefresh := .T.

   if oData != nil
      if hb_HHASKEY( ::hRows, oData:cRow )
         hData = ::hRows[ oData:cRow ][ PLANN_ROW_HASH ]
         if hb_HHASKEY( hData, oData:cName )
            hb_HDEL( hData, oData:cName )
            if lRefresh
               ::Refresh()
            endif
         endif
      endif 
   endif

RETURN NIL

//----------------------------------------------------------------------------// 

METHOD Destroy() CLASS TPlanning 

   ::oHeaderFont:End()
   ::oDataFont:End()
   ::oFont_aux:End()
   ::oHeaderFont_aux:End()
   if ::oCursorCatch != NIL 
      ::oCursorCatch:end()
   endif
   DeleteObject( ::hConRight )
   DeleteObject( ::HConLeft )

return ::Super:Destroy() 

//----------------------------------------------------------------------------// 

METHOD GetAtCol( nCol ) CLASS TPlanning

   local nAtCol := 0

   if nCol > ::nLeftLabelWidth .and. ::pHitTest:nRow > ::nTopMargin
      nAtCol = AScan( ::aLabelHeader, {|x| x["LEFT"] < nCol .AND. x["RIGHT"] >= nCol } )
   endif

return nAtCol

//----------------------------------------------------------------------------// 

METHOD GetAtRow( nRow ) CLASS TPlanning

   local nAtRow
   
   nAtRow := Int( ( nRow + ::nVirtualTop - ::nTopMargin - ::nHeaderHeight ) / ::nRowHeight ) + 1
   
   if nAtRow > Len( ::hRows ) .OR. nRow < ( ::nTopMargin + ::nHeaderHeight )
      nAtRow = 0 
   endif

return nAtRow

//----------------------------------------------------------------------------// 

METHOD GetCol( nAtCol ) CLASS TPlanning

   local nCol
   local nModCol   := ::ModCol()
   local nColWidth := ::ColWidth()
   local nAux      := ( nColWidth + 1 ) * nModCol   

   if nAtCol > nModCol
      nCol = nAux + ( ( nAtCol - nModCol - 1 ) * nColWidth ) + ::nLeftLabelWidth
   else 
      nCol = ( nAtCol - 1 ) * ( nColWidth + 1 ) + ::nLeftLabelWidth
   endif
   
   nCol := Max( ::nLeftLabelWidth - 1, nCol )

return nCol

//----------------------------------------------------------------------------// 

METHOD GetCoorFromPos( nAtRow, nAtCol ) CLASS TPlanning

   local aCoor := Array( 4 )
   local nCol
   
   aCoor[ 1 ] = ::GetRow( nAtRow ) + 1
   aCoor[ 2 ] = ::GetCol( nAtCol ) + 1
   aCoor[ 3 ] = aCoor[ 1 ] + ::nRowHeight - 1
   aCoor[ 4 ] = ::GetCol( nAtCol + 1 )

return aCoor

//----------------------------------------------------------------------------// 

METHOD GetData( nAtRow, nCol ) CLASS TPlanning
   local oData, oRet
   local hDatas
   local nAtCol := ::GetAtCol( nCol )
   local nColDataEnd, nColDataStart
   local nColWidth

   if ::lNoHalfDay
      nColWidth = 0
   else
      nColWidth = ( ::ColWidth() / 2 )
   endif

   if nAtRow < 1 .Or. nAtCol < 1
      return nil 
   endif 
    
   hDatas = hb_HGET( ::hRows, hb_HKEYAT( ::hRows, nAtRow ) )[ PLANN_ROW_HASH ]
   for each oData in hDatas
#ifdef __XHARBOUR__
      oData = oData:Value
#endif     
      if oData != NIL .and. ( oData:nColStart != NIL .OR.  oData:nColEnd != nil )
         if oData:nColStart <= nCol .AND. oData:nColEnd >= nCol
            oRet = oData
            exit
         endif
      endif
   next
   

return oRet

//----------------------------------------------------------------------------// 


METHOD GetRow( nAtRow ) CLASS TPlanning

   local nRow 
   
   nRow = ( nAtRow - 1 ) * ::nRowHeight - ::nVirtualTop + ::nTopMargin + ::nHeaderHeight
   
return nRow

//----------------------------------------------------------------------------// 

METHOD GetRowText( nAtRow ) CLASS TPlanning
   
   local cText := ""

   DEFAULT nAtRow := 0
  
   if ::oLastData != NIL 
      cText = ::hRows[ ::oLastData:cRow ][ PLANN_ROW_TEXTO ] 
   else
      if Len( ::aSelected ) > 0 
         nAtRow = ::aSelected[ 1 ][ PLANN_ATROW ]
      endif 
   endif 
  
   if nAtRow > 0
      cText = ::hRows[ hb_HKEYAT( ::hRows, nAtRow ) ][ PLANN_ROW_TEXTO ]
   endif
   
return cText

//----------------------------------------------------------------------------// 

METHOD GetRowID( nAtRow ) CLASS TPlanning
   
   local uID
   
   if nAtRow == NIL
      if ::oLastData != NIL .and. ::oLastData:lSelected
         uID = ::oLastData:cRow
      else 
         if nAtRow == NIL 
            if Len( ::aSelected ) > 0 
               nAtRow = ::aSelected[ 1 ][ PLANN_ATROW ]
            else 
               nAtRow = 0
            endif 
         endif
         if nAtRow > 0
             uID = hb_HKEYAT( ::hRows, nAtRow )
         endif
      endif
   else
      if nAtRow > 0
         uID = hb_HKEYAT( ::hRows, nAtRow )
      endif
   endif
   
return uID

//----------------------------------------------------------------------------// 

METHOD HandleEvent( nMsg, nWParam, nLParam ) CLASS TPlanning

   do case
      case nMsg == WM_MOUSELEAVE
         return ::MouseLeave( nHiWord( nLParam ), nLoWord( nLParam ), nWParam )
  endcase

RETURN ::Super:HandleEvent( nMsg, nWParam, nLParam )

//----------------------------------------------------------------------------// 

METHOD HitTest( nRow, nCol ) CLASS TPlanning

   local pHitTest := TStruct():New()
   local nAtRow, nAtCol
   local pLast
   
   pHitTest:Addmember( "nType",  _INT )
   pHitTest:Addmember( "nAtRow", LONG )
   pHitTest:Addmember( "nAtCol", LONG )
   pHitTest:Addmember( "nRow", LONG )
   pHitTest:Addmember( "nCol", LONG )
   pHitTest:Addmember( "nMoveType", _INT )

   pHitTest:nAtRow = 0
   pHitTest:nAtCol = 0
   pHitTest:nRow = nRow
   pHitTest:nCol = nCol
   
   pLast = ::pHitTest
   ::pHitTest = pHitTest

   if nRow < ::nTopMargin 
      pHitTest:nType  = PLANN_HT_NONE
      ::nDaysOffset     = 0
      ::nRoomsOffset     = 0
   elseif nRow > ::nTopMargin .and. nRow < ::nTopMargin + ::nHeaderHeight
      pHitTest:nType  = PLANN_HT_HEADER
      ::nDaysOffset     = 0
      ::nRoomsOffset     = 0
   else
      pHitTest:nType  = PLANN_HT_BODY
      nAtRow = ::GetAtRow( nRow )
      nAtCol = ::GetAtCol( nCol )

      if pLast != NIL
         pHitTest:nMoveType = PLANNING_MOVE_NONE
         if nAtRow > pLast:nAtRow
            pHitTest:nMoveType = PLANNING_MOVE_DOWN
         elseif nAtRow <  pLast:nAtRow
            pHitTest:nMoveType = PLANNING_MOVE_UP
         endif
         ::nRoomsOffset += ( nAtRow - pLast:nAtRow )
         if nAtCol > pLast:nAtCol
            pHitTest:nMoveType = PLANNING_MOVE_RIGTH
         elseif nAtCol <  pLast:nAtCol
            pHitTest:nMoveType = PLANNING_MOVE_LEFT
         endif
         ::nDaysOffset += ( nAtCol - pLast:nAtCol )
      endif
      pHitTest:nAtRow = nAtRow 
      pHitTest:nAtCol = nAtCol
   endif

return pHitTest

//----------------------------------------------------------------------------// 

METHOD LButtonDown( nRow, nCol, nKeyFlags ) CLASS TPlanning
   
   ::SelectCell( nRow, nCol, nKeyFlags )
   ::nDaysOffset     = 0
   ::nRoomsOffset     = 0

return ::Super:LButtonDown( nRow, nCol, nKeyFlags )

//----------------------------------------------------------------------------// 

METHOD LButtonUp( nRow, nCol, nKeyFlags ) CLASS TPlanning
   
   local uId
   local nAt 
   ::lCaptured = .F.
   if ::oLastData != NIL
      if ::oLastData:lCatched  
         ::oLastData:lCatched = .F.
         if hb_isBlock( ::bOnCapture ) 
            if ( nAt := ::GetAtRow( nRow ) ) > 0 
               uId = ::GetRowId( nAt )
               if ::DataDropAvailable( uId, ::oLastData )
                  Eval( ::bOnCapture, ::oLastData, uId, self )
               endif
            endif
         endif
      endif
      if ::oLastData != NIL 
         if ::oLastData:lResized
            ::oLastData:lResized = .F.
            if hb_isBlock( ::bOnResizedData ) 
               if ( nAt := ::GetAtRow( nRow ) ) > 0 
                  uId = ::GetRowId( nAt )
                  if ::DataDropAvailable( uId, ::oLastData )
                     if ::oLastData:lRFromStart
                        if ::oLastData:dStart + ::nDaysOffset >= ::oLastData:dEnd
                           ::nDaysOffset = ::oLastData:dEnd - ::oLastData:dStart - 1
                        endif
                     else
                        if ::oLastData:dEnd + ::nDaysOffset <= ::oLastData:dStart
                           ::nDaysOffset = ::oLastData:dStart - ::oLastData:dEnd + 1
                        endif
                     endif
                     Eval( ::bOnResizedData, ::oLastData, uId, self )
                  endif
               endif
            endif
         endif
      endif
   endif
   ::nDaysOffset     = 0
   ::nRoomsOffset     = 0
   CursorArrow()
   ::Refresh()

return ::Super:LButtonUp( nRow, nCol, nKeyFlags )

//----------------------------------------------------------------------------// 

METHOD Line( hDC, nTop, nLeft, nBottom, nRight, nColor ) CLASS TPlanning

   local hPen, hOldPen
   
   DEFAULT nColor := 0
   
   hPen = CreatePen( PS_SOLID, 1, nColor )
   hOldPen = SelectObject( hDC, hPen )
   MoveTo( hDC, nLeft, nTop )
   LineTo( hDC, nRight, nBottom )
   SelectObject( hDC, hOldPen )   
   DeleteObject( hPen )
   

return nil

//-----------------------------------------------------------------// 

METHOD MouseLeave( nRow, nCol, nFlags ) CLASS TPlanning

   ::nDaysOffset     = 0
   ::nRoomsOffset     = 0
   if ::oLastData != NIL 
      ::oLastData:lCatched = .F. 
      ::oLastData:lResized = .F. 
   endif 
   ::Refresh() 
return 0

//-----------------------------------------------------------------// 

METHOD MouseMove( nRow, nCol, nKeyFlags ) CLASS TPlanning

   local pHitTest
   local nColWidth := ::ColWidth()
   local oData
   local nEastSize
   local nWestSize

   TrackMouseEvent( ::hWnd, TME_LEAVE )

   pHitTest = ::HitTest( nRow, nCol )

   oData = ::GetData( pHitTest:nAtRow, nCol )
  
   if ::oLastData == NIL .OR. ( ! ::oLastData:lCatched .AND. ! ::oLastData:lResized )
      if oData != NIL 
         if ::oDataOver != NIL 
            if ::oDataOver:nID == oData:nID
               if ::cToolTip == NIL
                  ::DestroyToolTip()
                  ::cToolTip = oData:cToolTip 
               endif
            else 
               ::DestroyToolTip()
               ::cToolTip = NIL 
            endif 
         endif
      else 
         ::DestroyToolTip()
         ::cToolTip = NIL    
      endif
   
      ::oDataOver = oData

      if oData != NIL 
         if ( ( oData:nColStart <= nCol .AND. oData:nColStart + 2 >= nCol ) .OR. ;
           ( oData:nColEnd >= nCol .AND. oData:nColEnd - 2 <= nCol ) ) 
            CursorWE()
            return 0
         endif
      endif 

      if pHitTest:nType == PLANN_HT_BODY
         ::Refresh()
      endif

   else
      if ::oLastData != NIL
         if pHitTest:nAtCol == 0 .OR. pHitTest:nAtRow == 0
            ::oLastData:lResized = .f.
            ::oLastData:lCatched = .f.
            ::Refresh()
         endif
         if ::oLastData:lCatched
            if ::oCursorCatch != NIL
               SetCursor( ::oCursorCatch:hCursor )
            else
               CursorCatch()
            endif
            if pHitTest:nMoveType > 0
               ::Refresh()
            endif
            return 0
         endif

         if ::oLastData:lResized
            CursorWE()
            if pHitTest:nMoveType > 0
               ::Refresh()
            endif
            return 0
         endif
      endif

   endif

return ::Super:MouseMove( nRow, nCol, nKeyFlags )

//----------------------------------------------------------------------------// 

METHOD MouseWheel( nKeys, nDelta, nXPos, nYPos ) CLASS TPlanning

   local aPos := { nYPos, nXPos }

   if ::lSBVisible   
      if nDelta < 0 
         ::VScrollSkip( 40 )
      else 
         ::VScrollSkip( -40 )
      endif
   endif

return nil

//----------------------------------------------------------------------------// 

METHOD Paint() CLASS TPlanning

   local aInfo       := ::DispBegin() 
   local aRect 
   local hDC         := ::hDC
   local nGridWidth  := ::GridWidth()
   local nGridHeight := ::GridHeight() + ::nTopMargin
   
   aRect = GetClientRect( ::hWnd )
   
   FillRect( hDC, GetClientRect( ::hWnd ), ::oBrush:hBrush )

   WndBox2007( hDC, ::nTopMargin,;
               ::nLeftMargin, ;
               nGridHeight, ;
               nGridWidth , ;
               ::nColorGrid )

   ::PaintDates( hDC )
   
   ::DispEnd( aInfo ) 

return 0 

//----------------------------------------------------------------------------// 

STATIC function CalPos( oData, Self, nDaysOffset )

   local u := {=>}
   local nTotDays :=  Len(::aLabelHeader)
   local nColWidth_2
   local nColWidth

   DEFAULT nDaysOffset := 0

   nColWidth = ::ColWidth()

   nColWidth_2 = 0// Int( nColWidth / 2 )


   u["lPrev"] = .F.
   u["lNext"] = .F.
   if oData:lResized
      if oData:lRFromStart
         u["nColStart"] = Max( 1, oData:dStart - ::dStart + 1 + nDaysOffset )
         u["nColEnd"]   = Min( nTotDays, max( 1, oData:dEnd - ::dStart + If( ::lNoHalfDay, 0, 1 ) ) )
         if u["nColStart"] >= u["nColEnd"]
            u["nColStart"] = u["nColEnd"] - If( ::lNoHalfDay, 0, 1 )
         endif
      else 
         u["nColStart"] = Max( 1, oData:dStart - ::dStart + 1 )
         u["nColEnd"]   = Min( nTotDays, max( 1, oData:dEnd - ::dStart + If( ::lNoHalfDay, 0, 1 ) + nDaysOffset ) )
         if u["nColStart"] >= u["nColEnd"]
            u["nColEnd"] = u["nColStart"]  + If( ::lNoHalfDay, 0, 1 )
         endif
      endif
      if ! ::lNoHalfDay
         nColWidth_2 = ( ::aLabelHeader[u["nColStart"]]["RIGHT"] - ::aLabelHeader[u["nColStart"]]["LEFT"] ) / 2
      endif
      if oData:lRFromStart
         u["nCol1"] = ::aLabelHeader[u["nColStart"]]["LEFT"] + nColWidth_2
         if oData:dEnd > ::dEnd
            u["nCol2"] = ::aLabelHeader[u["nColEnd"]]["RIGHT"]
            u["lNext"] = .T.
         else
            u["nCol2"] = ::aLabelHeader[u["nColEnd"]]["RIGHT"] - nColWidth_2
         endif
      else 
         u["nCol2"] = ::aLabelHeader[u["nColEnd"]]["RIGHT"] - nColWidth_2
         if oData:dStart < ::dStart
            u["nCol1"] = ::aLabelHeader[u["nColStart"]]["LEFT"]
            u["lPrev"] = .T.
         else
            u["nCol1"] = ::aLabelHeader[u["nColStart"]]["LEFT"] + nColWidth_2
         endif
      endif
   else
      u["nColStart"] = Max( 1, oData:dStart - ::dStart + 1 + nDaysOffset )
      u["nColEnd"]   = Min( nTotDays, max( 1, oData:dEnd - ::dStart + If( ::lNoHalfDay, 0, 1 ) + nDaysOffset ) )
   endif

   if ! ::lNoHalfDay
      nColWidth_2 = ( ::aLabelHeader[u["nColStart"]]["RIGHT"] - ::aLabelHeader[u["nColStart"]]["LEFT"] ) / 2
   endif

   if ! oData:lResized
      if oData:dStart + nDaysOffset < ::dStart
         u["lPrev"] = .T.
         u["nCol1"] = ::aLabelHeader[u["nColStart"]]["LEFT"]
      else 
         u["nCol1"] = ::aLabelHeader[u["nColStart"]]["LEFT"] + nColWidth_2
      endif
      if oData:dEnd + nDaysOffset > ::dEnd
         u["lNext"] = .T.
         u["nCol2"] = ::aLabelHeader[u["nColEnd"]]["RIGHT"] 
      else
         u["nCol2"] = ::aLabelHeader[u["nColEnd"]]["RIGHT"] - nColWidth_2
      endif
   endif

return u

//----------------------------------------------------------------------------// 

METHOD PaintData( hDC ) CLASS TPlanning

   local oData, hDatas
   local Row, hPen, hPenRed
   local nStartRow, nEndRow
   local n, j
   local nRow, nColStart, nColEnd
   local nModCol, nColWidth, nAux
   local nCol1, nCol2
   local aClrData
   local nClrTextData 
   local lPrev := .F., lNext := .F.
   local hCatched := NIL
   local nColWidth_2 := 0
   local nTotDays :=  Len(::aLabelHeader)
   local u

   nColWidth = ::ColWidth()
   
   nStartRow = ::FirstVisibleRow()
   nEndRow   = ::LastVisibleRow()
   

      
   for n = nStartRow to nEndRow
      hDatas = hb_HGET( ::hRows, hb_HKEYAT( ::hRows, n ) )[ PLANN_ROW_HASH ]
      nRow = ::GetRow( n )
      for each oData in hDatas
#ifdef __XHARBOUR__
         oData = oData:Value
#endif      

         u = CalPos(oData, self)
         
         lPrev = u["lPrev"]
         lNext = u["lNext"]
         nCol1 = u["nCol1"]
         nCol2 = u["nCol2"]

         oData:nColStart = nCol1
         oData:nColEnd = nCol2
   
         aClrData = Eval( ::bClrData, oData )
         
         GradientFill( hDC, nRow + 1, nCol1 + 1, nRow + ::nRowHeight - If( ::lVertGrad, 1, 0 ), nCol2 - 1, aClrData, ::lVertGrad )

         if lPrev
            if nCol1 > nBmpWidth( ::hConLeft )
               DrawTransparent( hDC, ::hConLeft, nRow + ( ::nRowHeight / 2 - nBmpHeight( ::hConLeft ) / 2  ), ;
                                      ::nLeftLabelWidth - 1 + 3 )
            endif
         endif
         
         if lNext := ( ::GridWidth()  < nCol2 + 1 )
            if nColWidth > nBmpWidth( ::hConRight )
               DrawTransparent( hDC, ::hConRight, nRow + ( ::nRowHeight / 2 - nBmpHeight( ::hConRight ) / 2  ), ;
                                     ::GridWidth() - nBmpWidth( ::hConRight ) - 1 )
            endif
         endif
         
         nClrTextData = Eval( ::bClrTextData, oData )
         
         P_Say( hDC, oData:cDescribe, ;
                    { nRow ,;
                      nCol1 + 7 + If( lPrev,  nBmpWidth( ::hConLeft ) + 2, 0 ), ;
                      nRow + ::nRowHeight, ;
                      nCol2 - 7 - If( lNext,  nBmpWidth( ::hConLeft ) + 2, 0 ) }, nOr( DT_SINGLELINE, DT_VCENTER ), ::oDataFont, nClrTextData, 0 )
         
         WndBox2007( hDC, nRow ,;
               nCol1 + 1, ;
               nRow + ::nRowHeight, ;
               nCol2 - 1, ;
               0 )
         if oData:lSelected                
            WndBox2007( hDC, nRow + 1,;
                  nCol1 + 2, ;
                  nRow + ::nRowHeight - 1, ;
                  nCol2 - 2, ;
                  0 )
         endif      
         if oData:lCatched .OR. oData:lResized
            hCatched = {=>}
            hCatched["DATA"] = oData
            hCatched["COL1"] = nCol1
            hCatched["COL2"] = nCol2
            hCatched["ROW"]  = nRow
            hCatched["ATROW"] = n
         endif   
      next
   next
   if hCatched != NIL      
      nRow = hCatched["ROW"]

      u = CalPos( hCatched["DATA"], self, ::nDaysOffset )

      nColStart = u["nColStart"]
      nColEnd   = u["nColEnd"]

      nCol1 = u["nCol1"]
      nCol2 = u["nCol2"]

      if u["lPrev"]
         DrawTransparent( hDC, ::hConLeft, nRow + ( ::nRowHeight / 2 - nBmpHeight( ::hConLeft ) / 2  )  + (::nRoomsOffset*::nRowHeight), ;
                                ::nLeftLabelWidth + 2 )
      endif

      if u["lNext"]
         DrawTransparent( hDC, ::hConRight, nRow + ( ::nRowHeight / 2 - nBmpHeight( ::hConRight ) / 2  )  + (::nRoomsOffset*::nRowHeight), ;
                               ::GridWidth() - nBmpWidth( ::hConRight ) - 1 )
      endif

      hCatched["DATA"]:nNewColStart = nCol1 + 1
      hCatched["DATA"]:nNewColEnd = nCol2 - 1

      if ::DataDropAvailable( ::GetRowId( hCatched["ATROW"] + ::nRoomsOffset ), hCatched["DATA"])
         hPen = CreatePen( PS_DOT, 1, 0 )
      else 
         hPen = CreatePen( PS_DOT, 1, nRGB(255,0,0) )
      endif

      WNDBOXCLR( hDC, nRow + 2 + (::nRoomsOffset*::nRowHeight),;
                  nCol1 + 2, ;
                  nRow + ::nRowHeight - 2 +  (::nRoomsOffset*::nRowHeight), ;
                  nCol2 - 2, hPen, hpen)

      DeleteObject( hPen )
   endif

return nil

//----------------------------------------------------------------------------// 

METHOD PaintDates( hDC ) CLASS TPlanning	

   local nTotDays := Len( ::aLabelHeader )
   local n, hSel
   local nMod
   local nColWidth
   local nColOffSet
   local aRect
   local nClr
   local cRowText
   local nRowOffSet
   local nStartRow, nEndRow
   local aSelected, aCell
   local nFrom, nTo, head
   
   nColWidth  = ::ColWidth()
   nMod       = ::ModCol()
   nColOffSet = ::nLeftLabelWidth 
 
   nMod      = ::ModCol()   
   nColOffSet   = ::nLeftLabelWidth       

   for n = 1 to nTotDays
      nColOffSet += ( nColWidth + If( nMod > 0, ( nMod--, 1 ), 0 ) ) 
      if n == 1
         ::aLabelHeader[n]["LEFT"] = ::nLeftLabelWidth
         ::aLabelHeader[n]["RIGHT"] = nColOffSet
      else 
         ::aLabelHeader[n]["LEFT"] = ::aLabelHeader[n-1]["RIGHT"]
         ::aLabelHeader[n]["RIGHT"] = nColOffSet
      endif 
      if n == nTotDays
         ::aLabelHeader[n]["LEFT"] = ::aLabelHeader[n-1]["RIGHT"]
         ::aLabelHeader[n]["RIGHT"] = ::GridWidth()
      endif
      ::Line( hDC, ::nTopMargin , nColOffSet, ::nHeight - 3 , nColOffSet, ::nColorGrid )
   next   

   GradientFill( hDC, ::nTopMargin + 1, ::nLeftMargin + 1, ::nHeight - 5 , ::nLeftLabelWidth, ::aGradLabel )


   // visibles rows
   nStartRow = ::FirstVisibleRow()
   nEndRow   = ::LastVisibleRow()

   for n = nStartRow to nEndRow
      cRowText = hb_HGET( ::hRows, hb_HKEYAT( ::hRows, n ) )[ PLANN_ROW_TEXTO ]
      nRowOffSet = ::nTopMargin + ::nHeaderHeight + ( ::nRowHeight * ( n - 1 ) )
      if ::pHitTest != NIL 
         if ::pHitTest:nAtRow == n 
            P_Say( hDC, cRowText, { nRowOffSet - ::nVirtualTop, ::nLeftMargin + 1, nRowOffSet + ::nRowHeight - ::nVirtualTop, ::nLeftLabelWidth },;
          nOR( DT_VCENTER, DT_SINGLELINE ), ::oFont_aux, nClr, 0 )
         else 
            P_Say( hDC, cRowText, { nRowOffSet - ::nVirtualTop, ::nLeftMargin + 1, nRowOffSet + ::nRowHeight - ::nVirtualTop, ::nLeftLabelWidth },;
          nOR( DT_VCENTER, DT_SINGLELINE ), ::oFont, nClr, 0 )
         endif 
      else
         P_Say( hDC, cRowText, { nRowOffSet - ::nVirtualTop, ::nLeftMargin + 1, nRowOffSet + ::nRowHeight - ::nVirtualTop, ::nLeftLabelWidth },;
          nOR( DT_VCENTER, DT_SINGLELINE ), ::oFont, nClr, 0 )
      endif
      
      ::Line( hDC, nRowOffSet + ::nRowHeight - ::nVirtualTop, ::nLeftMargin, nRowOffSet + ::nRowHeight - ::nVirtualTop, ::nWidth - ::nRightMargin - 1 - ::nVScroll, ::nColorGrid )    
   next

   
   //Detail column (header)
   hSel = CreateSolidBrush( ::nColorCellSelected )
   
   if ::lCaptured
      if ::pHitTest:nAtCol < ::nColDown
         nFrom = ::pHitTest:nAtCol
         nTo   = ::nColDown
      elseif ::pHitTest:nAtCol > ::nColDown
         nFrom = ::nColDown
         nTo   = ::pHitTest:nAtCol
      else 
         nFrom = ::nColDown
         nTo   = ::nColDown
      endif
      ::aSelected = {}
      for n = nFrom to nTo
         aCell = ::GetCoorFromPos( ::nRowDown, n )
         FillRect( hDC, aCell, hSel )
         AAdd( ::aSelected, { ::nRowDown, n } )
      next      
   else
      for each aSelected in ::aSelected
         aCell = ::GetCoorFromPos( aSelected [ PLANN_ATROW ], aSelected [ PLANN_ATCOL ] )      
         FillRect( hDC, aCell, hSel )
      next            
   endif
   
   DeleteObject( hSel )
   //Paint datas
   ::PaintData( hDC )

   GradientFill( hDC, ::nTopMargin , ;
                     ::nLeftLabelWidth + 1, ;
                     ::nTopMargin + ::nHeaderHeight - 1, ;
                     ::GridWidth(), ::aGradHeaderCel )



   //Header Horizontal Line
   ::Line( hDC, ::nTopMargin + ::nHeaderHeight-1, ::nLeftMargin, ::nTopMargin + ::nHeaderHeight-1, ::GridWidth(), ::nColorGrid )


   for each head in ::aLabelHeader
      aRect = { ::nTopMargin + 2, head["LEFT"],;
                ::nTopMargin + ::nHeaderHeight - 2, head["RIGHT"] }

#ifdef __XHARBOUR__
         n = HB_EnumIndex()
#else
         n = head:__enumIndex()
#endif

      if ::bClrLabelHeader != NIL 
         nClr = Eval( ::bClrLabelHeader, ::dStart + n - 1 )
      else 
         nClr = ::nClrText
      endif 

      if ::pHitTest != NIL 
         if ::pHitTest:nAtCol == n
            P_Say( hDC, head['LABEL'], aRect, DT_CENTER, ::oHeaderFont_aux, nClr, 0 )
         else 
            P_Say( hDC, head['LABEL'], aRect, DT_CENTER, ::oHeaderFont, nClr, 0 )
         endif 
      else
         P_Say( hDC, head['LABEL'], aRect, DT_CENTER, ::oHeaderFont, nClr, 0 )
      endif

   next
   
   GradientFill( hDC, ::nTopMargin, ;
                     ::nLeftMargin + 1, ;
                     ::nTopMargin + ::nHeaderHeight - 1, ;
                     ::nLeftLabelWidth - 1, ::aGradHeaderCel )
   
   
                  
   P_Say( hDC, ::cHeader, { ::nTopMargin + 2, ::nLeftMargin + 1, ::nTopMargin + ::nHeaderHeight - 1, ::nLeftLabelWidth },;
          nOR( DT_VCENTER, DT_CENTER, DT_SINGLELINE ), ::oHeaderFont, nClr, 0 )

   FillRect( hDC, { 0, ;
   	                0,;
   	                ::nTopMargin,;
   	                ::GridWidth() }, ::oBrush:hBrush )
  	                
   WndBox2007( hDC, ::nTopMargin,;
               ::nLeftMargin, ;
               ::nHeaderHeight + ::nTopMargin -1, ;
               ::nLeftLabelWidth , ;
               ::nColorGrid )

                   
   for each head in ::aLabelHeader
      ::Line( hDC, ::nTopMargin , head["LEFT"], ::nTopMargin + ::nHeaderHeight , head["LEFT"], ::nColorGrid )
   next 

return NIL

//----------------------------------------------------------------------------// 

METHOD RButtonUp( nRow, nCol, nKeyFlags ) CLASS TPlanning
   
   local nInit := 0, nEnd := 0
   local lOK := .F.

    if nRow < ::nTopMargin+30  .OR. nCol < ::nLeftLabelWidth .OR. ::GetAtRow( nRow ) < 1
       return nil                              // agregado para evitar el click derecho en cabecera
    endif                                     //


   if ::oLastData != NIL 
      if ::oDataOver != NIL 
         if ::oDataOver:nID != ::oLastData:nID
            ::oLastData:lSelected = .F.
            ::oLastData = ::oDataOver
            ::oLastData:lSelected = .T.
            ::Refresh()
         endif
         if ( lOK := ::oLastData:lSelected )
            nInit = ::oLastData:dStart - ::dStart
            nEnd  = ::oLastData:dEnd - ::dStart
         endif
      endif
   endif
   
   if ! lOK
      if ::oDataOver != NIL 
         ::oLastData = ::oDataOver
         ::oLastData:lSelected = .T.
         nInit = ::oLastData:dStart - ::dStart
         nEnd  = ::oLastData:dEnd - ::dStart               
         lOK = .T.
         ::Refresh()
      endif   
      if ! lOK 
         if Len( ::aSelected ) > 0
            if ::pHitTest:nAtRow == ::aSelected[ 1 ][ PLANN_ATROW ]
               if ::pHitTest:nAtCol >= ::aSelected[ 1 ][ PLANN_ATCOL ] .and. ;
                  ::pHitTest:nAtCol <= Atail( ::aSelected )[ PLANN_ATCOL ]
               else 
                  ::SelectCell( nRow, nCol )
               endif
            else
               ::SelectCell( nRow, nCol )
            endif
         else 
            ::SelectCell( nRow, nCol )
         endif
         if ! lOK 
            nInit = ::aSelected[ 1 ][ PLANN_ATCOL ] - 1
            nEnd  = Atail( ::aSelected )[ PLANN_ATCOL ]     
         endif
      endif
   endif

   ::lCaptured = .F.
   
   if ::bRSelected != NIL 
      Eval( ::bRSelected, nRow, nCol, Self, ;
                          ::dStart + nInit, ;
                          ::dStart + nEnd  )
   endif

return ::Super:RButtonUp( nRow, nCol, nKeyFlags )

//----------------------------------------------------------------------------// 

METHOD Reset() CLASS TPlanning
   
   local aRow 
   local n
   local cKey
   
   for n = 1 to Len( ::hRows )
      cKey = hb_HKEYAT( ::hRows, n )
      hb_HSET( ::hRows, cKey, { {=>}, hb_HGET( ::hRows, cKey )[ PLANN_ROW_TEXTO ] } )
   next

return NIL

//----------------------------------------------------------------------------// 

METHOD SelectCell( nRow, nCol ) CLASS TPlanning

   local nAtRow := ::GetAtRow( nRow )
   local nAtCol := ::GetAtCol( nCol )
   local oData

   ::nRowDown  = nAtRow
   ::nColDown  = nAtCol
   ::aSelected = {}
   
   if ::oLastData != NIL 
      ::oLastData:lSelected = .F.
   endif   
   
   oData = ::GetData( nAtRow, nCol )
   
   if oData == NIL
      ::lCaptured = .T.
      AAdd( ::aSelected, { nAtRow, nAtCol } )
   else
      oData:lSelected = .T.
      if ( ( oData:nColStart <= nCol .AND. oData:nColStart + 2 >= nCol ) .OR. ;
           ( oData:nColEnd >= nCol .AND. oData:nColEnd - 2 <= nCol ) )
         oData:lResized = .T.
         oData:lRFromStart = oData:nColStart <= nCol .AND. oData:nColStart + 2 >= nCol 
      else
         oData:lCatched = .T.
      endif
      ::oLastData = oData
      ::cToolTip = NIL
   endif
   
   ::Refresh()

return NIL

//----------------------------------------------------------------------------// 

METHOD SetLabelHeader() CLASS TPlanning

   local n, dDate, cLabel
   local nTotDays := ::dEnd - ::dStart + 1
   local hHead
   
   ::aLabelHeader = {}

   for n = 1 to nTotDays
      hHead := {=>}
      dDate = ::dStart + ( n - 1 )  
      cLabel = SubStr( CDoW( dDate ), 1, 3 ) + CRLF + StrZero( Day( dDate ), 2 ) + "-" + SubStr( CMonth( dDate ), 1, 3 )
      hHead["LABEL"] = cLabel
      hHead["DATE"]  = DToS(dDate)
      hHead["LEFT"]  = 0 
      hHead["RIGHT"] = 0
      AAdd( ::aLabelHeader, hHead )    
   next
   
return nil

//----------------------------------------------------------------------------// 

METHOD SetScroll() CLASS TPlanning

   ::oVScroll:bGoUp     = {|| ::VScrollSkip( - ::nRowHeight ) }
   ::oVScroll:bGoDown   = {|| ::VScrollSkip( ::nRowHeight ) }
   ::oVScroll:bPageUp   = {|| ::VScrollSkip( - ::oVScroll:nPgStep ) }
   ::oVScroll:bPageDown = {|| ::VScrollSkip( ::oVScroll:nPgStep ) }
   ::oVScroll:bPos      = {|nPos| ::VScrollSetPos( nPos ) }
   ::oVScroll:bTrack    = {|nPos| ::VScrollSetPos( nPos ) }

return nil

//----------------------------------------------------------------------------// 

METHOD SetDates( dStart, dEnd ) CLASS TPlanning

   DEFAULT dStart := ::dStart
   DEFAULT dEnd   := ::dEnd

   if dStart >= dEnd
      return nil 
   endif
   
   ::dStart = dStart
   ::dEnd   = dEnd
   ::Reset()
   ::SetLabelHeader()
   ::Refresh()
   
return nil

//----------------------------------------------------------------------------//

METHOD UpdateData( cRow, dStart, dEnd, cDescribe, cToolTip ) CLASS TPlanning


return nil

//----------------------------------------------------------------------------//

METHOD VScrollSetPos( nPos ) CLASS TPlanning

   local nSkip := nPos - ::nVirtualTop 

   ::nVirtualTop := nPos
   ::oVScroll:SetPos( nPos )

   ::Refresh()
   

RETURN nil

//----------------------------------------------------------------------------//

METHOD VScrollSkip( nSkip ) CLASS TPlanning

   local nHeight := ( ::nVirtualHeight - ::nHeight )
   local nAux

   IF (::nVirtualTop == 0 .And. nSkip < 0) .Or. ;
      (::nVirtualTop == nHeight .And. nSkip > 0)
      RETURN nil
   ENDIF

   nAux = ::nVirtualTop
   ::nVirtualTop += nSkip

   ::nVirtualTop = Min( ::nVirtualHeight - ::nHeight, ::nVirtualTop )

   IF ::nVirtualTop < 0
      ::nVirtualTop := 0
   ELSEIF ::nVirtualTop > nHeight
      ::nVirtualTop := nHeight
   ENDIF
   ::oVScroll:SetPos( ::nVirtualTop )

   ::Refresh()
   
   if nAux - ::nVirtualTop != -nSkip
      nSkip = -( nAux - ::nVirtualTop )
   endif
   

RETURN nil

//----------------------------------------------------------------------------// 

FUNCTION P_GetLastDayMonth( dDate )
   
   local nMonth, cDay, nYear
   local dAux
   
   nMonth = Month( dDate )
   cDay   = "01"
   nYear  = Year( dDate )
   if nMonth == 12
      nMonth = 1 
      nYear++
   else 
      nMonth++
   endif 
   
   dAux = ( SToD( StrZero( nYear, 4 ) + StrZero( nMonth, 2 ) + cDay ) ) - 1

return dAux


//----------------------------------------------------------------------------//
//****************************************************************************//
//----------------------------------------------------------------------------//


CLASS TPData

   DATA oPlanning
   DATA cRow
   DATA cName
   DATA dStart
   DATA dEnd
   DATA Cargo
   DATA cDescribe
   DATA cToolTip
   DATA nID
   DATA nColStart, nColEnd
   DATA nNewColStart, nNewColEnd
   
   DATA lSelected
   DATA lCatched
   DATA lResized, lRFromStart, lRFromEnd
   
   METHOD New()

   METHOD GetNewId()   

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( oPlanning, cRow, dStart, dEnd, cDescribe, cToolTip ) CLASS TPData

   DEFAULT dStart := Date(),;
           dEnd   := Date()
           
   ::oPlanning = oPlanning
   ::cRow      = cRow
   ::cName     = DToS( dStart )
   ::dStart    = dStart
   ::dEnd      = dEnd 
   ::cDescribe = cDescribe
   ::cToolTip  = cToolTip
   
   ::lSelected = .F.
   ::lCatched = .F.
   ::lResized = .F.
   ::lRFromStart = .F.
   ::lRFromEnd = .F.
   ::nID = ::GetNewId()

return Self

//----------------------------------------------------------------------------// 

METHOD GetNewId() CLASS TPData

   nID++
   
   if nID > 10000
      nID = 1 
   endif

return nID


//----------------------------------------------------------------------------//
//****************************************************************************//
//----------------------------------------------------------------------------//


#define TRANSPARENT         0x1   //1
//----------------------------------------------------------------------------//

static function DrawTransparent( hDC, hBmp, nRow, nCol )

   local hDCMem 
   local hBmpOld 
   local nZeroZeroClr

   hDCMem = CreateCompatibleDC( hDC )      
      
   // we can not get nZeroZeroClr from hDC is possible hDC are locked by other SelectObject
   // An application cannot select a bitmap into more than one device context at a time.   
   hBmpOld      = SelectObject( hDCMem, hBmp )
   nZeroZeroClr = GetPixel( hDCMem, 0, 0 )

   SelectObject( hDCMem, hBmpOld )
   DeleteDC( hDCMem )

   TransBmp( hBmp, nBmpWidth( hBmp ), nBmpHeight( hBmp ),;
             nZeroZeroClr, hDC, nCol, nRow, nBmpWidth( hBmp ), nBmpHeight( hBmp ) )

return nil

//----------------------------------------------------------------------------//
                            
static FUNCTION P_Say( hDC, cText, aRect, nStyle, oFont, nClrText, nClrBack )

   local nOldMode := SetBkMode( hDC, 1 )
   local nOldClr  := SetTextColor( hDC, nClrText )
   local hOldFont 
   
   if oFont != NIL
      hOldFont = SelectObject( hDC, oFont:hFont )
   endif

   DrawText( hDC, cText, aRect, nStyle )
   SetBkMode( hDC, nOldMode )     
   SetTextColor( hDC, nOldClr )     
   if oFont != NIL
      SelectObject( hDC, hOldFont )
   endif   

return nil
