#include <hbapi.h> 
#include <windows.h>

HBITMAP CreateMemBitmap( HDC, LPSTR );

static far BYTE nextitem [] = {
0x42, 0x4D, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00,
0x28, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x01, 0x00, 0x18, 0x00,
0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA,
0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA,
0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA,
0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA,
0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA,
0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA };                      

static far BYTE previtem [] = {
0x42, 0x4D, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00,
0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
0x00, 0x00, 0x0A, 0x01, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00,
0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6,
0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA, 0xE6, 0x00, 0xFA } ;   

//----------------------------------------------------------------------------// 

HB_FUNC( HCONRIGHT )
{
   hb_retnl( ( LONG ) CreateMemBitmap( 0, ( LPSTR ) nextitem ) );
}  

//----------------------------------------------------------------------------// 

HB_FUNC( HCONLEFT )
{
   hb_retnl( ( LONG ) CreateMemBitmap( 0, ( LPSTR ) previtem ) );
}  
