#ifndef _PLANNING_H_
#define _PLANNING_H_

#define PLANNING_MOVE_NONE  0
#define PLANNING_MOVE_RIGTH 1
#define PLANNING_MOVE_LEFT  2
#define PLANNING_MOVE_UP    3
#define PLANNING_MOVE_DOWN  4

#xcommand DEFINE PLANNING <oPlann>;
          [ <of: OF, WINDOW, DIALOG, PANEL> <oWnd> ];
          [ HEADER <cHeader> ];
          [ START DAY <dStart> ];
          [ END DAY <dEnd> ];
          [ LABEL WIDTH <nLabelWidth> ];
          [ TOP MARGIN <nTopMargin> ];
          [ FONT <oFont> ];
          [ FONT DATA <oDataFont> ];
          [ FONT HEADER <oHeaderFont> ];
          [ COLOR HEADER <bClrLabelHeader> ];
          [ COLOR CELL <bClrData>] ;
          [ COLOR TEXT <bClrTextData> ];
          [ ON RIGHT SELECT <bRSelected> ] ;
          [ ON CAPTURE <bOncapture> ];
          [ ON RESIZE DATA <bOnResize> ];
          [ <lNoHalfDay: NOHALFDAY> ] ;
       => ;
           <oPlann> := TPlanning():New( <oWnd>, <oFont>, <oDataFont>, <oHeaderFont>, <dStart>, <dEnd>,  <cHeader>, <.lNoHalfDay.> );;
           WITH OBJECT <oPlann>;;
              [ :bClrLabelHeader := \{ | dDate | <bClrLabelHeader> \} ] ;;
              [ :bClrData := \{ | oData | <bClrData> \} ] ;;
              [ :bClrTextData := \{ | oData | <bClrTextData> \} ] ;;
              [ :bRSelected := \{ | nRow, nCol, Self, dCheckIn, dCheckOut | <bRSelected> \} ] ;;
              [ :bOnCapture := \{ | oData, nRowId, Self | <bOncapture> \ } ];;
              [ :bOnResizedData := \{ | oData, nRowId, Self | <bOnResize> \ } ];;
          END <oPlann>

#endif
